module cz.zcu.fav.kiv.ups.prochazm {
    requires javafx.controls;
    requires javafx.fxml;
    requires org.controlsfx.controls;

    opens cz.zcu.fav.kiv.ups.prochazm to javafx.fxml;
    opens  cz.zcu.fav.kiv.ups.prochazm.controllers to javafx.fxml;

    exports cz.zcu.fav.kiv.ups.prochazm;
}