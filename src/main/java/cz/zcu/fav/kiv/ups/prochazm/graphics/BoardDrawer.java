package cz.zcu.fav.kiv.ups.prochazm.graphics;

import cz.zcu.fav.kiv.ups.prochazm.tetris.IGame;
import javafx.scene.canvas.GraphicsContext;

public class BoardDrawer implements IDrawer {

    private static final double WIDTH = BASE * 10;
    private static final double HEIGHT = BASE * 20;

    private final IGame game;
    private final TetrominoDrawer tetrominoDrawer;
    private final MessageDrawer messageDrawer;

    BoardDrawer(IGame game){
        this.game = game;
        this.tetrominoDrawer = new TetrominoDrawer(game);
        this.messageDrawer = new MessageDrawer(game::getState, game::getTime);
    }

    @Override
    public void draw(GraphicsContext context, double x, double y) {
        for (int tileY = 0; tileY < IGame.BOARD_HEIGHT_VISIBLE; ++tileY){
            for (int tileX = 0; tileX < IGame.BOARD_WIDTH; ++tileX){
                context.drawImage(
                    SpriteManager.spriteOf(this.game.getTile(tileX, tileY + IGame.BOARD_HEIGHT_HIDDEN)),
                    tileX * BASE + x,
                    tileY * BASE + y
                );
            }
        }

        this.tetrominoDrawer.draw(context, x, y);
        this.messageDrawer.draw(context, x, y);
    }

    @Override
    public double getWidth() {
        return WIDTH;
    }

    @Override
    public double getHeight() {
        return HEIGHT;
    }
}
