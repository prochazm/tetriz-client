package cz.zcu.fav.kiv.ups.prochazm.engine;

import cz.zcu.fav.kiv.ups.prochazm.tetris.Game;
import cz.zcu.fav.kiv.ups.prochazm.graphics.*;
import cz.zcu.fav.kiv.ups.prochazm.model.beans.GameBean;
import cz.zcu.fav.kiv.ups.prochazm.model.beans.PlayerBean;
import javafx.animation.AnimationTimer;
import javafx.beans.property.SimpleLongProperty;
import javafx.scene.canvas.GraphicsContext;

import java.util.ArrayList;
import java.util.List;


public class Engine {

    private final AnimationTimer timer;
    private final GameBean gameBean;
    private final SimpleLongProperty localGameTime;

    private long previousFrameTime;


    public Engine(GraphicsContext context, GameBean gameBean){
        this.localGameTime = new SimpleLongProperty();
        this.gameBean = gameBean;

        CompositeDrawer compositeDrawer = (
            gameBean.getRoomId() != 0
            ? initGames()
            : initGame((int)(Math.random() * 99999), gameBean.getPlayers().get(0))
        );

        compositeDrawer.include(new TimerDrawer(
                this.localGameTime::get,
                () -> ceil((int)(gameBean.getStartTime() - System.currentTimeMillis()), 1000)
            ), compositeDrawer.getWidth() / 2, 0);



        this.timer = new AnimationTimer() {
            @Override
            public void handle(long now) {
                long increment = now - previousFrameTime;
                if (increment < 30000000){
                    return;
                }

                context.clearRect(0, 0,
                    context.getCanvas().getWidth(),
                    context.getCanvas().getHeight()
                );

                compositeDrawer.draw(
                    context,
                    (context.getCanvas().getWidth() - compositeDrawer.getWidth()) / 2,
                    10
                );


                if (!gameBean.isFinished() && gameBean.isStarted()){
                    localGameTime.set(localGameTime.getValue() + increment);
                }

                previousFrameTime = now;
            }
        };
    }

    private int ceil(int x, int y){
        return (x + y - 1) / y;
    }

    private CompositeDrawer initGame(long seed, PlayerBean playerBean) {
        playerBean.gameProperty().set(new Game(seed, this.localGameTime));
        return new CompositeDrawer().include(new GameDrawer(playerBean), 0, 40);
    }

    private CompositeDrawer initGames() {
        List<IDrawer> gameDrawers = new ArrayList<>();

        this.gameBean.getLocalPlayer()
            .gameProperty()
            .set(new Game(this.gameBean.getSeed(), this.localGameTime));

        gameDrawers.add(new GameDrawer(this.gameBean.getLocalPlayer()));

        this.gameBean.getPlayers()
            .stream()
            .filter(player -> !player.equals(this.gameBean.getLocalPlayer()))
            .forEach(player -> {
                player.gameProperty().set(new Game(this.gameBean.getSeed()));
                gameDrawers.add(new GameDrawer(player));
            });

        return CompositeDrawerFactory.multiPlayerRoom(gameDrawers);
    }

    public void setTime(long time) {
        this.localGameTime.set(time);
    }

    public void start(){
        this.previousFrameTime = System.nanoTime();
        this.timer.start();
    }

    public void stop(){
        this.timer.stop();
    }

    public long getTime() {
        return this.localGameTime.get();
    }
}
