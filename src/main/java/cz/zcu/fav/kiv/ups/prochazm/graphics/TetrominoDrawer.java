package cz.zcu.fav.kiv.ups.prochazm.graphics;

import cz.zcu.fav.kiv.ups.prochazm.tetris.IGame;
import cz.zcu.fav.kiv.ups.prochazm.tetris.ITetromino;
import javafx.scene.canvas.GraphicsContext;

public class TetrominoDrawer implements IDrawer {

    private final IGame game;

    public TetrominoDrawer(IGame game) {
        this.game = game;
    }

    @Override
    public void draw(GraphicsContext context, double x, double y) {
        ITetromino tetromino = this.game.getPiece();
        int xPos = tetromino.getX();
        int yPos = tetromino.getY() - IGame.BOARD_HEIGHT_HIDDEN;

        context.save();

        for (int blockY = 0; blockY < tetromino.getHeight(); ++blockY){
            for (int blockX = 0; blockX < tetromino.getWidth(); ++blockX) {
                if (tetromino.getBlockAt(blockX, blockY)) {
                    double xAdjusted = (xPos + blockX) * BASE;
                    double yAdjusted = (yPos + blockY) * BASE;
                    double yAdjustedGhost = yAdjusted + this.game.getGhost() * BASE;

                    if (yAdjustedGhost >= 0){
                        context.drawImage(SpriteManager.spriteOfGhost(tetromino), xAdjusted + x, yAdjustedGhost + y);
                    }

                    if (yAdjusted >= 0){
                        context.drawImage(SpriteManager.spriteOf(tetromino), xAdjusted + x, yAdjusted + y);
                    }
                }
            }
        }
        context.restore();
    }

    @Override
    public double getWidth() {
        return BASE * 4;
    }

    @Override
    public double getHeight() {
        return BASE * 4;
    }
}
