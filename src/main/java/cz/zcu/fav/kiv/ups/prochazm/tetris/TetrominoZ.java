package cz.zcu.fav.kiv.ups.prochazm.tetris;

public class TetrominoZ extends Tetromino {

    private final Type type = Type.Z;
    private boolean[][] boundingBox = {
        {true , true , false},
        {false, true , true },
        {false, false, false}
    };


    @Override
    public Type getType() {
        return type;
    }

    @Override
    boolean[][] getBoundingBox() {
        return this.boundingBox;
    }

    @Override
    void setBoundingBox(boolean[][] boundingBox) {
        this.boundingBox = boundingBox;
    }
}
