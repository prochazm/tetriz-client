package cz.zcu.fav.kiv.ups.prochazm.model;

import cz.zcu.fav.kiv.ups.prochazm.tetris.GameAction;
import cz.zcu.fav.kiv.ups.prochazm.model.beans.GameBean;
import cz.zcu.fav.kiv.ups.prochazm.model.beans.PlayerBean;

import java.util.List;

public class LocalGameManager implements IGameManager {

    @Override
    public List<Move> queryGameAction(String playerName, long index) {
        return null;
    }

    @Override
    public GameBean getGameForRoom(long roomID, String localName) {
        GameBean gameBean = new GameBean(roomID);

        PlayerBean playerBean = new PlayerBean("Local player", false);
        gameBean.getPlayers().add(playerBean);
        gameBean.setLocalPlayer(playerBean);
        gameBean.setStartTime(System.currentTimeMillis() + 3500);

        return gameBean;
    }

    @Override
    public void sendGameAction(GameAction gameAction) {

    }

    @Override
    public boolean leave() {
        return true;
    }

    @Override
    public long getRoomTime() {
        return 0;
    }
}
