package cz.zcu.fav.kiv.ups.prochazm.tetris;

public class TetrominoT extends Tetromino {

    private final Type type = Type.T;
    private boolean[][] boundingBox = {
        {false, true , false},
        {true , true , true },
        {false, false, false}
    };


    @Override
    public Type getType() {
        return type;
    }

    @Override
    boolean[][] getBoundingBox() {
        return this.boundingBox;
    }

    @Override
    void setBoundingBox(boolean[][] boundingBox) {
        this.boundingBox = boundingBox;
    }
}
