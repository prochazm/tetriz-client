package cz.zcu.fav.kiv.ups.prochazm.graphics;

import cz.zcu.fav.kiv.ups.prochazm.model.beans.PlayerBean;
import javafx.scene.canvas.GraphicsContext;

public class PlayerInfoDrawer implements IDrawer{

    private final double WIDTH = BASE * 15;

    private final PlayerBean playerBean;

    PlayerInfoDrawer(PlayerBean playerBean) {
        this.playerBean = playerBean;
    }

    @Override
    public void draw(GraphicsContext context, double x, double y) {
        context.save();
        context.setFont(FONT_MONOSPACE_SMALLER);
        double x_OFFSETS = 5;
        context.fillText(this.playerBean.getName(), x + x_OFFSETS, y + 20);
        context.fillText(this.playerBean.isActive() ? "🖧" : "✕", x + WIDTH - x_OFFSETS, y + 20);
        context.restore();
    }

    @Override
    public double getWidth() {
        return WIDTH;
    }

    @Override
    public double getHeight() {
        return BASE;
    }
}
