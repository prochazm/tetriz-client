package cz.zcu.fav.kiv.ups.prochazm.graphics;

import cz.zcu.fav.kiv.ups.prochazm.tetris.Game;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

import java.time.Duration;
import java.util.function.Supplier;

public class MessageDrawer extends DurationPrinter implements IDrawer {

    private static final double WIDTH = BASE * 10;
    private static final double HEIGHT = BASE * 4;

    private static final Font FONT = new Font("Monospaced", 24);
    private final Supplier<Game.State> stateSupplier;

    public MessageDrawer(Supplier<Game.State> stateSupplier, Supplier<Duration> timeSupplier) {
        super(timeSupplier);
        this.stateSupplier = stateSupplier;
    }

    @Override
    public void draw(GraphicsContext context, double x, double y) {
        switch (stateSupplier.get()) {
            case FINISHED:
                String message = "FINISHED\n".concat(super.getTimestamp());
                drawMessage(context, message, x, y);
                break;
            case FAILED:
                drawMessage(context, "GAME OVER", x, y);
                break;
        }
    }

    @Override
    public double getWidth() {
        return WIDTH;
    }

    @Override
    public double getHeight() {
        return HEIGHT;
    }

    private void drawMessage(GraphicsContext context, String message, double x, double y){
        context.save();
        context.setFill(Color.gray(1., .8));
        context.fillRect(x, y + BASE * 8, WIDTH, HEIGHT);
        context.setTextAlign(TextAlignment.CENTER);
        context.setTextBaseline(VPos.CENTER);
        context.setFill(Color.BLACK);
        context.setFont(FONT);
        context.fillText(message,x + BASE * 5,y + WIDTH);
        context.restore();
    }
}
