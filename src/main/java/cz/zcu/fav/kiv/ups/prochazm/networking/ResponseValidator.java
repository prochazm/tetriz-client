package cz.zcu.fav.kiv.ups.prochazm.networking;

import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class ResponseValidator {

    private final Response response;

    private ResponseValidator(Response response){
        this.response = response;
    }

    public static ResponseValidator forResponse(Response response){
        return new ResponseValidator(response);
    }

    private void forEachLong(Consumer<Long> longConsumer) {
        this.response.getLongTokens().forEach(longConsumer);
    }

    private void forEachString(Consumer<String> stringConsumer) {
        this.response.getStringTokens().forEach(stringConsumer);
    }

    public ArityMatchingPhase tryMatch(TokenArity longArity, TokenArity stringArity) {
        return new ArityMatchingPhase(matches(longArity, stringArity), this);
    }

    public ArityMatchingPhase tryMatch(TokenArity tokenArity) {
        return new ArityMatchingPhase(
            matches(tokenArity, tokenArity)
                && this.response.getLongTokens().size() == this.response.getStringTokens().size(),
            this
        );
    }

    private boolean matches(TokenArity longArity, TokenArity stringArity) {
        return Objects.nonNull(this.response) &&
            (matchTokenArity(this.response.getLongTokens(), longArity)
                && matchTokenArity(this.response.getStringTokens(), stringArity)
                || !this.response.getSuccess()
                && this.response.getLongTokens().size() == 0
                && this.response.getStringTokens().size() == 0
            );
    }

    private static boolean matchTokenArity(List<?> tokens, TokenArity arity) {
        switch (arity) {
            case NONE: return tokens.size() == 0;
            case ONE:  return tokens.size() == 1;
            case TWO:  return tokens.size() == 2;
            case MANY: return tokens.size() >= 1;
            case ANY:
            default:   return true;
        }
    }

    public enum TokenArity {
        NONE, ONE, TWO, MANY, ANY
    }


    public class ArityMatchingPhase {

        private final boolean result;
        private final ResponseValidator responseValidator;


        ArityMatchingPhase(boolean result, ResponseValidator responseValidator){
            this.result = result;
            this.responseValidator = responseValidator;
        }

        public long ifMatchesGetLong(boolean success, int i){
            if (this.result && success == response.getSuccess()) {
                return response.getLongToken(i);
            }
            return 0;
        }


        public ArityMatchingPhase ifMatchesRunOnLongs(boolean success, Consumer<Long> longConsumer) {
            if (this.result && success) {
                forEachLong(longConsumer);
            }
            return this;
        }

        public ArityMatchingPhase ifMatchesRunOnStrings(boolean success, Consumer<String> stringConsumer) {
            if (this.result && success) {
                forEachString(stringConsumer);
            }
            return this;
        }

        public ArityMatchingPhase ifMatchesRunOnTokens(boolean success, BiConsumer<Long, String> tokenConsumer) {
            if (this.result && success) {
                Iterator<Long> longs = response.getLongTokens().iterator();
                Iterator<String> strings = response.getStringTokens().iterator();
                while (longs.hasNext()) {
                    tokenConsumer.accept(longs.next(), strings.next());
                }
            }
            return this;
        }

        public ArityMatchingPhase ifMatchesRun(boolean success, Consumer<Response> responseConsumer){
            if (this.result && success) {
                responseConsumer.accept(response);
            }
            return this;
        }

        public ArityMatchingPhase onMismatch(Consumer<Response> responseConsumer) {
            if (!this.result) {
                responseConsumer.accept(response);
            }
            return this;
        }

        public ResponseValidator then() {
            return this.responseValidator;
        }

        public boolean isMatchAndSuccess() {
            return Objects.nonNull(response) && response.getSuccess() && this.result;
        }
    }

}
