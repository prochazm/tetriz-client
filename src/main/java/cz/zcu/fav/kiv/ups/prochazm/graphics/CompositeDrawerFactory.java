package cz.zcu.fav.kiv.ups.prochazm.graphics;

import java.util.List;

public class CompositeDrawerFactory {

    private CompositeDrawerFactory() { }


    public static CompositeDrawer multiPlayerRoom(List<IDrawer> games){
        CompositeDrawer compositeDrawer = new CompositeDrawer();
        compositeDrawer.include(games.get(0), 0d, 40d);
        for (int i = 0; i < games.size() - 1; ++i){
            compositeDrawer.include(
                games.get(i + 1),
                16 * IDrawer.BASE * (1 + i % 3),
                40 + (double)(i / 3) * IDrawer.BASE * 22
            );
        }
        return compositeDrawer;
    }
}
