package cz.zcu.fav.kiv.ups.prochazm.controllers;

import cz.zcu.fav.kiv.ups.prochazm.model.Model;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import org.controlsfx.validation.Severity;
import org.controlsfx.validation.ValidationSupport;
import org.controlsfx.validation.Validator;
import org.controlsfx.validation.decoration.StyleClassValidationDecoration;

public class MenuController extends AController {

    private static final String ERROR_CLASS = "error";
    private static final String DEFAULT_CONNECT_BTN_TEXT = "Connect";
    private static final String FAILURE_CONNECT_BTN_TEXT = "Failed";
    private static final String REFUSED_CONNECT_BTN_TEXT = "Refused";

    private static String defaultAddress = "";
    private static String defaultName = "";

    @FXML
    public Button practiceBtn;

    @FXML
    private Button connectBtn;

    @FXML
    private TextField addressTextField;

    @FXML
    private TextField loginTextField;

    @FXML
    private VBox root;


    public MenuController(Model model) {
        super(model);
    }

    public MenuController(Model model, String address, String name){
        super(model);
        defaultAddress = address;
        defaultName = name;
    }

    @FXML
    public void initialize() {
        Platform.runLater(() -> root.requestFocus());

        this.connectBtn.focusedProperty().addListener((b, c, d) -> {
            if (c && !d) { // was focused but it's not anymore
                connectBtn.getStyleClass().remove(ERROR_CLASS);
                connectBtn.setText(DEFAULT_CONNECT_BTN_TEXT);
            }
        });

        // setup input validator
        ValidationSupport validator = new ValidationSupport();
        validator.setValidationDecorator(
            new StyleClassValidationDecoration(ERROR_CLASS, ""));
        validator.registerValidator(this.addressTextField,
            Validator.createRegexValidator("", "[\\w.]+:\\d+", Severity.ERROR));
        validator.registerValidator(this.loginTextField,
            Validator.createRegexValidator("", "^\\p{ASCII}+$", Severity.ERROR));
        validator.initInitialDecoration();

        // don't even allow connection with crappy inputs
        this.connectBtn.disableProperty().bind(validator.invalidProperty());
        this.addressTextField.setText(defaultAddress);
        this.loginTextField.setText(defaultName);
    }

    @FXML
    private void gotoPractice() {
        redirect(GAME_CONTROLLER, 0L);
    }

    @FXML
    private void tryConnect() {
        Platform.runLater(() -> {
            String[] split = this.addressTextField.getText().split(":");
            super.model.getProtocol().setServer(split[0], Integer.parseInt(split[1]));
            if (!super.model.getProtocol().getConnection().connect()) { // disconnected
                this.connectBtn.setText(FAILURE_CONNECT_BTN_TEXT);
                this.connectBtn.getStyleClass().add(ERROR_CLASS);
            } else { // connected
                // try login -> move to lobby if succeeds
                if (super.model.getProtocol().login(this.loginTextField.getText())) {
                    super.model.setName(this.loginTextField.getText());
                    // remember login info
                    defaultName = this.loginTextField.getText();
                    defaultAddress = this.addressTextField.getText();
                    // redirect to lobby
                    redirect(LOBBY_CONTROLLER);
                } else { // connected but login failed (i.e. user collision)
                    this.connectBtn.setText(REFUSED_CONNECT_BTN_TEXT);
                    this.connectBtn.getStyleClass().add(ERROR_CLASS);
                }
            }
        });
    }
}
