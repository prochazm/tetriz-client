package cz.zcu.fav.kiv.ups.prochazm.tetris;

public class TetrominoL extends Tetromino {

    private final Type type = Type.L;
    private boolean[][] boundingBox = {
        {false, false, true },
        {true , true , true },
        {false, false, false}
    };


    @Override
    public Type getType() {
        return type;
    }

    @Override
    public boolean[][] getBoundingBox() {
        return this.boundingBox;
    }

    @Override
    void setBoundingBox(boolean[][] boundingBox) {
        this.boundingBox = boundingBox;
    }
}
