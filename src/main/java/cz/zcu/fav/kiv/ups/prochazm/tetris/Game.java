package cz.zcu.fav.kiv.ups.prochazm.tetris;


import cz.zcu.fav.kiv.ups.prochazm.tetris.Tetromino.Type;
import javafx.beans.property.SimpleLongProperty;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import static cz.zcu.fav.kiv.ups.prochazm.tetris.Direction.*;

public class Game implements IGame {
    private static final Duration TICK_LENGTH = Duration.ofSeconds(1);
    private static final int LOCK_DELAY_LIMIT = 15;
    private static final int ROWS_CLEARED_GOAL = 40;

    private final RandomGenerator bag;
    private final Type[][] board;

    private Tetromino swap;
    private Tetromino piece;
    private State state;
    private boolean justSwapped;
    private int ghostOffset;
    private int rowsCleared;
    private int lockDelayCounter;
    private boolean autoDrop;

    private Duration time = Duration.ZERO;
    private SimpleLongProperty timer;

    private final List<TickListener> tickListeners;

    private Game(long seed, SimpleLongProperty timer, boolean autoDrop){
        this.tickListeners = new ArrayList<>();
        this.bag = new RandomGenerator(seed);
        this.board = new Type[BOARD_HEIGHT][BOARD_WIDTH];
        this.state = State.READY;
        this.timer = timer;
        this.autoDrop = autoDrop;

        timer.addListener((s, o, n) -> {
            if (this.autoDrop && this.state == State.PLAYING) {
                long ns = n.longValue() - o.longValue();
                this.time = this.time.plusNanos(ns);
                while (this.time.compareTo(TICK_LENGTH) > 0){
                    this.time = this.time.minus(TICK_LENGTH);
                    this.tickListeners.forEach(TickListener::onTick);
                    this.lockDelayCounter = 0;
                    moveDown();
                }
            } else if (this.state == State.READY && n.longValue() != 0) {
                this.state = State.PLAYING;
            }
        });

        spawn();
    }
    public Game(long seed, SimpleLongProperty timer) {
        this(seed, timer, true);
    }

    public Game(long seed) {
        this(seed, new SimpleLongProperty(0), false);
    }

    @Override
    public void addTickListener(TickListener eventListener){
        this.tickListeners.add(eventListener);
    }

    @Override
    public State getState(){
        return this.state;
    }

    @Override
    public ITetromino getPiece(){
        return this.piece;
    }

    @Override
    public ITetromino getHold() {
        return this.swap;
    }

    @Override
    public Duration getTime(){
        return Duration.ofNanos(this.timer.get());
    }

    @Override
    public Type getTile(int x, int y){
        return Objects.requireNonNullElse(this.board[y][x], Type.VOID);
    }

    @Override
    public int getGhost(){
        return this.ghostOffset;
    }

    @Override
    public int getRowsCleared(){
        return Math.min(this.rowsCleared, ROWS_CLEARED_GOAL);
    }

    @Override
    public Iterator<ITetromino> getTetrominoBag(){
        return this.bag.iterator();
    }

    @Override
    public void swap(){
        if (this.justSwapped){
            return;
        }

        if (this.swap == null){
            this.swap = this.piece.getType().getTetromino();
            spawn();
        } else {
            Tetromino tmp = this.piece.getType().getTetromino();
            spawn(this.swap);
            this.swap = tmp;
        }

        this.justSwapped = true;
    }

    @Override
    public void moveLeft(){
        if (isEmpty(-1, 0)){
            this.piece.translate(LEFT);
        }
        refreshGhost();
        lockDelay();
    }

    @Override
    public void moveRight(){
        if (isEmpty(1, 0)){
            this.piece.translate(RIGHT);
        }
        refreshGhost();
        lockDelay();
    }

    @Override
    public void moveDown(){
        if (isEmpty(0, 1)) {
            this.piece.translate(DOWN);
        } else {
            lockDown();
            spawn();
        }

        refreshGhost();
    }

    @Override
    public void drop(){
        this.piece.translateDown(this.ghostOffset);
        moveDown();
    }

    @Override
    public void rotate(){
        boolean succeeded = true;
        this.piece.rotate();
        if (!isEmpty(0, 0)){
            succeeded = false;
            for (int i = 0; i < KickTable.KICK_COUNT; i++){
                int xOffset = KickTable.getOffsetX(this.piece, i);
                int yOffset = KickTable.getOffsetY(this.piece, i);
                if (isEmpty(xOffset, yOffset)){
                    this.piece.translate(xOffset, yOffset);
                    succeeded = true;
                    break;
                }
            }
        }
        if (!succeeded){
            this.piece.undoRotation();
        }
        refreshGhost();
        lockDelay();
    }

    @Override
    public void setTime(long timestamp){
        this.timer.set(timestamp);
    }

    @Override
    public void setAutoDrop(boolean autoDrop) {
        this.autoDrop = autoDrop;
    }

    private void lockDown(){
        if (this.piece.getY() + this.piece.getTopOffset() <= 1){
            this.state = State.FAILED;
        }

        merge();
        for (int i = this.piece.getY() + this.piece.getTopOffset();
             i < this.piece.getY() + this.piece.getSize() - this.piece.getBottomOffset();
             i++
        ) {
            if (isClearable(i)){
                clearLine(i);
                if (++this.rowsCleared == ROWS_CLEARED_GOAL){
                    this.timer = new SimpleLongProperty(this.timer.get());
                    this.state = State.FINISHED;
                }
            }
        }
    }

    private void lockDelay(){
        if (this.ghostOffset == 0 && this.lockDelayCounter < LOCK_DELAY_LIMIT){
            this.time = Duration.ZERO;
            this.lockDelayCounter++;
        }
    }

    private void spawn(){
        spawn((Tetromino) this.bag.poll());
    }

    private void spawn(Tetromino t){
        this.piece = t;
        this.piece.translate(3, 0);
        refreshGhost();
    }

    private void merge(){
        for (int x = 0; x < this.piece.getWidth(); x++) {
            for (int y = 0; y < this.piece.getHeight(); y++) {
                if (!offsetIsOutOfBound(x, y) && this.piece.getBlockAt(x, y)){
                    this.board[this.piece.getY() + y][this.piece.getX() + x] = this.piece.getType();
                }
            }
        }

        this.justSwapped = false;
    }

    private boolean isEmpty(int xOffset, int yOffset){
        final boolean[][] boundingBox = this.piece.getBoundingBox();
        final int size = this.piece.getSize();
        final int topOffset = this.piece.getTopOffset();
        final int bottomOffset = this.piece.getBottomOffset();
        final int leftOffset = this.piece.getLeftOffset();
        final int rightOffset = this.piece.getRightOffset();

        if (this.piece.getX() + leftOffset + xOffset < 0
            || this.piece.getX() + size - rightOffset + xOffset > BOARD_WIDTH
            || this.piece.getY() + topOffset + yOffset < 0
            || this.piece.getY() + size - bottomOffset + yOffset > BOARD_HEIGHT){
            return false;
        }

        for (int y = topOffset; y < size - bottomOffset; y++) {
            for (int x = leftOffset; x < size - rightOffset; x++) {
                if (boundingBox[y][x]
                    && Objects.nonNull(this.board[piece.getY() + y + yOffset][piece.getX() + x + xOffset])){
                    return false;
                }
            }
        }

        return true;
    }

    private boolean isClearable(int line){
        if (line < 0) { // can't be clearable if it's outside of game board
            return false;
        }

        for (int column = 0; column < BOARD_WIDTH; column++) {
            if (Objects.isNull(this.board[line][column])){
                return false;
            }
        }
        return true;
    }

    private void clearLine(int line){
        System.arraycopy(this.board, 1, this.board, 2, line - 1);
        this.board[1] = new Type[BOARD_WIDTH];
    }

    private boolean offsetIsOutOfBound(int xOffset, int yOffset){
        return piece.getY() + yOffset >= BOARD_HEIGHT
            || piece.getX() + xOffset >= BOARD_WIDTH
            || piece.getX() + xOffset < 0
            || piece.getY() + yOffset < 0;
    }

    private void refreshGhost(){
        this.ghostOffset = 0;

        while (isEmpty(0, this.ghostOffset)){
            this.ghostOffset++;
        }
        this.ghostOffset--;
    }

    public enum State {
        READY,
        PLAYING,
        FAILED,
        FINISHED;

        public boolean isDone() {
            return this.equals(FAILED) || this.equals(FINISHED);
        }
    }
}