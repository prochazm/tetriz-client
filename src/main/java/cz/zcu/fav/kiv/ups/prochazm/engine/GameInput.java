package cz.zcu.fav.kiv.ups.prochazm.engine;

import cz.zcu.fav.kiv.ups.prochazm.tetris.Game;
import cz.zcu.fav.kiv.ups.prochazm.tetris.GameAction;
import cz.zcu.fav.kiv.ups.prochazm.tetris.IGame;
import cz.zcu.fav.kiv.ups.prochazm.model.Move;

import java.util.Objects;

public class GameInput {

    private final IGame target;


    public GameInput(IGame target) {
        this.target = target;
    }


    public void notify(GameAction action) {
        if (Objects.isNull(action) || this.target.getState() != Game.State.PLAYING) {
            return;
        }

        switch (action) {
            case MOVE_LEFT:  this.target.moveLeft();   break;
            case MOVE_RIGHT: this.target.moveRight();  break;
            case ROTATE:     this.target.rotate();     break;
            case MOVE_DOWN:  this.target.moveDown();   break;
            case DROP:       this.target.drop();       break;
            case SWAP:       this.target.swap();       break;
        }
    }

    public void notify(Move move) {
        this.target.setTime(move.getTimestamp() * 1000000);
        notify(move.getAction());
    }
}
