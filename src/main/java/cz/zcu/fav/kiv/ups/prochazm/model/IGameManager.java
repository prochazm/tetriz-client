package cz.zcu.fav.kiv.ups.prochazm.model;

import cz.zcu.fav.kiv.ups.prochazm.tetris.GameAction;
import cz.zcu.fav.kiv.ups.prochazm.model.beans.GameBean;

import java.util.List;

public interface IGameManager {

    List<Move> queryGameAction(String playerName, long index);
    GameBean getGameForRoom(long roomID, String localName);
    void sendGameAction(GameAction gameAction);
    boolean leave();
    long getRoomTime();
}
