package cz.zcu.fav.kiv.ups.prochazm.tetris;

public enum Rotation {
    BASE, RIGHT, DOUBLE, LEFT;

    Rotation nextClockwise(){
        return nextByOrder(RIGHT, DOUBLE, LEFT, BASE);
    }

    Rotation nextAnticlockwise(){
        return nextByOrder(LEFT, BASE, RIGHT, DOUBLE);
    }

    private Rotation nextByOrder(Rotation a, Rotation b, Rotation c, Rotation d) {
        switch (this){
            case BASE:   return a;
            case RIGHT:  return b;
            case DOUBLE: return c;
            case LEFT:   return d;
            default:     return null;
        }
    }
}
