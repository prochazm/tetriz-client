package cz.zcu.fav.kiv.ups.prochazm.tetris;

import javafx.scene.input.KeyCode;

import java.util.HashMap;
import java.util.Map;

public enum GameAction {
    MOVE_LEFT,
    MOVE_RIGHT,
    MOVE_DOWN,
    DROP,
    ROTATE,
    SWAP,
    NONE;

    private static final Map<KeyCode, GameAction> keyboard_action_mapping = new HashMap<>();

    static {
        keyboard_action_mapping.put(KeyCode.LEFT, MOVE_LEFT);
        keyboard_action_mapping.put(KeyCode.A, MOVE_LEFT);
        keyboard_action_mapping.put(KeyCode.RIGHT, MOVE_RIGHT);
        keyboard_action_mapping.put(KeyCode.D, MOVE_RIGHT);
        keyboard_action_mapping.put(KeyCode.DOWN, MOVE_DOWN);
        keyboard_action_mapping.put(KeyCode.S, MOVE_DOWN);
        keyboard_action_mapping.put(KeyCode.UP, ROTATE);
        keyboard_action_mapping.put(KeyCode.W, ROTATE);
        keyboard_action_mapping.put(KeyCode.C, SWAP);
        keyboard_action_mapping.put(KeyCode.SHIFT, SWAP);
        keyboard_action_mapping.put(KeyCode.SPACE, DROP);
    }

    public static GameAction fromKeyCode(KeyCode keyCode){
        return keyboard_action_mapping.getOrDefault(keyCode, NONE);
    }
}
