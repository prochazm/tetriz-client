package cz.zcu.fav.kiv.ups.prochazm;

import cz.zcu.fav.kiv.ups.prochazm.controllers.AController;
import cz.zcu.fav.kiv.ups.prochazm.model.Model;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.*;

/**
 * JavaFX App
 */
public class App extends Application {

    private static Model model;
    private static Scene scene;
    private static Stage primaryStage;
    private static AController activeController;

    @Override
    public void start(Stage stage) {
        Map<String, String> args = getParameters().getNamed();

        primaryStage = stage;
        model = new Model();
        setRoot(
            AController.MENU_CONTROLLER,
            args.getOrDefault("host", ""),
            args.getOrDefault("name", "")
        );

        stage.setOnCloseRequest(e -> activeController.destroy());
        stage.setScene(scene);
        stage.show();

    }

    public static void setRoot(String fxml, Object... arguments) {
        try {
            if (Objects.isNull(scene)){
                scene = new Scene(loadFXML(fxml, arguments));
            } else {
                scene.setRoot(loadFXML(fxml, arguments));
            }
            Platform.runLater(() -> {
                primaryStage.setMinWidth(scene.getRoot().minWidth(-1));
                primaryStage.setMinHeight(scene.getRoot().minHeight(-1));
            });
        } catch (IOException | NoSuchMethodException e) {
            e.printStackTrace();
            Platform.exit();
        }
    }

    private static Parent loadFXML(String fxml, Object... arguments) throws IOException, NoSuchMethodException {
        FXMLLoader fxmlLoader = new FXMLLoader(App.class.getResource("controllers/" + fxml + ".fxml"));

        // todo this is horrible...
        List<Object> argList = new ArrayList<>();
        argList.add(model); // always pass model
        argList.addAll(Arrays.asList(arguments));

        fxmlLoader.setControllerFactory(param -> {
            Constructor<?>[] ctors = param.getConstructors();
            Object[] args = argList.toArray();
            int argc = args.length;

            main_loop:
            for (Constructor<?> ctor : ctors) {
                if (argc != ctor.getParameterCount()){
                    continue;
                }

                Class<?>[] ctorArgTypes = ctor.getParameterTypes();
                for (int i = 0; i < argc; ++i){
                    if (!ctorArgTypes[i].equals(args[i].getClass())){
                        continue main_loop;
                    }
                }

                try {
                    activeController = (AController) ctor.newInstance(argList.toArray());
                    return activeController;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            return null;
        });

        Parent instance = fxmlLoader.load();
        if (Objects.isNull(instance)){
            throw new NoSuchMethodException();
        }

        return instance;
    }

    public static void main(String[] args) {
        launch(args);
    }
}