package cz.zcu.fav.kiv.ups.prochazm.controllers;

import cz.zcu.fav.kiv.ups.prochazm.engine.Engine;
import cz.zcu.fav.kiv.ups.prochazm.engine.GameInput;
import cz.zcu.fav.kiv.ups.prochazm.engine.InputSource;
import cz.zcu.fav.kiv.ups.prochazm.tetris.Game;
import cz.zcu.fav.kiv.ups.prochazm.tetris.GameAction;
import cz.zcu.fav.kiv.ups.prochazm.model.*;
import cz.zcu.fav.kiv.ups.prochazm.model.beans.GameBean;
import cz.zcu.fav.kiv.ups.prochazm.networking.Connection;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.BorderPane;

import java.util.List;
import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.locks.ReentrantLock;

public class GameController extends AController {

    private final GameBean gameBean;
    private final IGameManager gameManager;
    private final Timer inputTimer;
    private final String returnControllerFxml;
    private final ReentrantLock moveOrderLock;

    private Engine engine;

    @FXML
    private BorderPane parent;

    @FXML
    private Canvas boardCanvas;


    public GameController(Model model, Long roomId) {
        super(model);

        if (roomId == 0) {
            this.gameManager = new LocalGameManager();
            this.returnControllerFxml = MENU_CONTROLLER;
        } else {
            this.gameManager = new GameManager(super.model.getProtocol());
            this.returnControllerFxml = LOBBY_CONTROLLER;

        }
        this.gameBean = this.gameManager.getGameForRoom(roomId, model.getName());
        this.inputTimer = new Timer();
        this.moveOrderLock = new ReentrantLock(true);
    }

    @FXML
    private void initialize() {
        // wire inputs and players
        this.boardCanvas.addEventHandler(KeyEvent.KEY_PRESSED, e -> {
            switch (e.getCode()) {
                case ESCAPE:
                case Q:
                    if (gameManager.leave()) {
                        redirect(this.returnControllerFxml);
                    }
                    break;
            }
        });

        this.engine = new Engine(
            this.boardCanvas.getGraphicsContext2D(),
            this.gameBean
        );

        this.gameBean.getPlayers().forEach(playerBean -> {
            GameInput gi = new GameInput(playerBean.getGame());
            if (this.gameBean.getLocalPlayer() == playerBean) {
                // recover from disconnect (if there was one...) and setup key bindings (only after recovery succeeds)
                new Thread(() -> {
                    List<Move> recovery = gameManager.queryGameAction(playerBean.getName(), 0);
                    if (Objects.nonNull(recovery) && recovery.size() > 0){
                        playerBean.getGame().setAutoDrop(false); // do NOT auto-drop while replaying
                        recovery.forEach(gi::notify);
                        playerBean.getGame().setAutoDrop(true); // game replayed, enable auto-drop
                        playerBean.getGame().addTickListener(() -> sendGameActionInitWorker(GameAction.MOVE_DOWN));
                        engine.setTime(gameManager.getRoomTime());
                    } else {
                        playerBean.getGame().addTickListener(() -> sendGameActionInitWorker(GameAction.MOVE_DOWN));
                    }

                    // input config
                    boardCanvas.addEventHandler(
                        KeyEvent.KEY_PRESSED,
                        e -> {
                            GameAction action = GameAction.fromKeyCode(e.getCode());
                            gi.notify(action);
                            sendGameActionInitWorker(action);
                        }
                    );
                }).start();


                // setup connection interrupt detection
                if (Objects.nonNull(this.model.getProtocol().getConnection())){
                    this.model.getProtocol().getConnection().addInterruptListener(
                        (s, o, n) -> playerBean.setActive(n == Connection.ConnectionState.CONNECTED)
                    );
                }
            } else {
                InputSource is = new InputSource(playerBean.getName(), this.gameManager);
                is.addMoveListener((s, o, n) -> n.forEach(gi::notify));
                is.addLastResponseConsumer(l -> playerBean.setActive(System.currentTimeMillis() - l < 3000));
                TimerTask fetchMovesTask = new TimerTask() {
                    @Override
                    public void run() {
                        if (playerBean.getGame().getState() == Game.State.FINISHED
                            || playerBean.getGame().getState() == Game.State.FAILED) {
                            this.cancel();
                        } else if (engine.getTime() > 0) { // local engine is ticking
                            is.run();
                        }
                    }
                };
                this.inputTimer.schedule(fetchMovesTask, 1, 500);
            }
        });

        this.parent.setMinWidth(40 + Math.min(this.gameBean.getPlayers().size() * 320, 1280));
        this.parent.setMinHeight(40 + (double)(this.gameBean.getPlayers().size() / 5 + 1) * 440);
        this.boardCanvas.widthProperty().bind(parent.widthProperty());
        this.boardCanvas.heightProperty().bind(parent.heightProperty());

        this.engine.start();
    }

    /**
     * This is internally synchronized, execution order
     * is guaranteed by moveOrderLock.
     *
     * @param gameAction action to be sent
     */
    private void sendGameActionInitWorker(GameAction gameAction) {
            new Thread(() -> {
                this.moveOrderLock.lock();
                try { // whatever happens, always unlock!
                    this.gameManager.sendGameAction(gameAction);
                } catch (Exception e){
                    e.printStackTrace();
                } finally {
                    this.moveOrderLock.unlock();
                }
            }).start();
    }


    @Override
    public void destroy() {
        super.destroy();
        this.engine.stop();
        this.inputTimer.cancel();
    }
}