package cz.zcu.fav.kiv.ups.prochazm.graphics;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;

public interface IDrawer {

    Font FONT_MONOSPACE = new Font("Tiwg Mono", 24);
    Font FONT_MONOSPACE_SMALLER = new Font("Tiwg Mono", 18);
    double BASE = 20.0;

    void draw(GraphicsContext context, double x, double y);

    double getWidth();

    double getHeight();
}
