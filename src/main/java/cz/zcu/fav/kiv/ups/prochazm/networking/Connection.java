package cz.zcu.fav.kiv.ups.prochazm.networking;

import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;

import java.io.*;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.util.Objects;

import static cz.zcu.fav.kiv.ups.prochazm.networking.Connection.ConnectionState.*;

public class Connection {

    private final static int RECONNECT_COUNT = 30;
    private final static int RECONNECT_DELAY_MS = 100;
    private final static double RECONNECT_DELAY_INCREASE = 0.6;

    private final SimpleObjectProperty<ConnectionState> connectedProperty;
    private final SimpleObjectProperty<ConnectionState> interruptedProperty;
    private final String address;
    private final int port;

    private Request resumeRequest;
    private boolean reconnect;
    private BufferedReader socketIn;
    private DataOutputStream socketOut;

    public enum ConnectionState {
        UNINITIATED("Uninitiated"),
        DISCONNECTED("Disconnected"),
        CONNECTED("Connected");

        private final String asString;

        ConnectionState(String connected) {
            this.asString = connected;
        }


        @Override
        public String toString() {
            return this.asString;
        }
    }

    public Connection(String address, int port) {
        this.connectedProperty = new SimpleObjectProperty<>(UNINITIATED);
        this.interruptedProperty = new SimpleObjectProperty<>(UNINITIATED);

        this.address = address;
        this.port = port;
        this.reconnect = true;
    }

    public void close() {
        try {
            System.out.println("Closing socket.");
            this.reconnect = false;
            this.socketIn.close();
            this.socketOut.close();
        } catch (IOException e) {
            System.out.println("Failed to close socket I/O stream");
        } finally {
            this.connectedProperty.set(DISCONNECTED);
        }
    }

    public void setResumeRequest(Request resumeRequest) {
        this.resumeRequest = resumeRequest;
    }

    synchronized public boolean connect() {
        try {
            Socket sock = new Socket();
            sock.connect(new InetSocketAddress(this.address, this.port), 2000);
            sock.setSoTimeout(1000);
            this.socketIn = new BufferedReader(new InputStreamReader(new DataInputStream(sock.getInputStream())));
            this.socketOut = new DataOutputStream(sock.getOutputStream());
        } catch (IOException e) {
            return false;
        }
        return true;
    }

    synchronized private Response sendWithRetry(Request request, int retries) {
        Response response = null;
        for (int i = retries; i > 0 && response == null; --i) {
            try {
                System.out.print(String.format("Sending: %s", new String(request.getBytes())));
                this.socketOut.write(request.getBytes());
                response = receive();
                this.connectedProperty.set(CONNECTED);
                this.interruptedProperty.set(CONNECTED);
            } catch (IOException | NullPointerException e) {
                if (!this.reconnect) {
                    break;
                }

                System.out.println(i > 1
                    ? String.format("Server disconnected, will retry %d more time(s)...", i - 1)
                    : String.format("[%s:%d] unreachable.", this.address, this.port));
                try {
                    this.interruptedProperty.set(connect() ? CONNECTED : DISCONNECTED);
                    if (this.interruptedProperty.get() == CONNECTED && Objects.nonNull(this.resumeRequest)) {
                        System.out.print("[Resume request] ");
                        System.out.print(String.format("Sending: %s", new String(resumeRequest.getBytes())));
                        Response resumeResponse;
                        try {
                            this.socketOut.write(resumeRequest.getBytes());
                            resumeResponse = receive();
                        } catch (IOException e2) {
                            resumeResponse = null;
                            System.out.println("[Resume request] failed");
                        }

                        if (resumeResponse != null && !resumeResponse.getSuccess()) {
                            break; // server responds F (it does not want client to reconnect)
                        }

                        // server did not respond or responded success
                        continue;
                    } else {
                        if (Objects.isNull(this.resumeRequest)) {
                            System.out.println("[Resume request undefined, skipping...]");
                        } else {
                            System.out.println("[Failed to reconnect]");
                        }
                    }
                    if (i > 1) {
                        Thread.sleep(RECONNECT_DELAY_MS * (long)(1. + RECONNECT_DELAY_INCREASE * (retries - i)));
                    }
                } catch (InterruptedException ie) {
                    // shouldn't happen
                    ie.printStackTrace();
                }
            }
        }

        this.connectedProperty.set(response == null ? DISCONNECTED : CONNECTED);
        return response;
    }

    synchronized Response send(Request request) {
        return sendWithRetry(request, RECONNECT_COUNT);
    }

    synchronized private Response receive() throws IOException {
        String received = this.socketIn.readLine();
        System.out.print(String.format("Receiving: %s\n", received));
        return new Response(Objects.requireNonNullElse(received, ""));
    }

    public void addInterruptListener(ChangeListener<ConnectionState> interruptListener) {
        this.interruptedProperty.addListener(interruptListener);
    }

    public void removeInterruptListener(ChangeListener<ConnectionState> interruptListener) {
        this.interruptedProperty.removeListener(interruptListener);
    }

    public void addDisconnectListener(ChangeListener<ConnectionState> disconnectListener) {
        this.connectedProperty.addListener(disconnectListener);
    }

    public void removeDisconnectListener(ChangeListener<ConnectionState> disconnectListener) {
        this.connectedProperty.removeListener(disconnectListener);
    }

}
