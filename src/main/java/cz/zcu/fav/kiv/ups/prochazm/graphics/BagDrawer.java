package cz.zcu.fav.kiv.ups.prochazm.graphics;

import cz.zcu.fav.kiv.ups.prochazm.tetris.ITetromino;
import javafx.scene.canvas.GraphicsContext;

import java.util.Iterator;
import java.util.function.Supplier;

public class BagDrawer implements IDrawer {
    private static final int BAG_SIZE = 4;
    private final Supplier<Iterator<ITetromino>> bagSupplier;


    BagDrawer(Supplier<Iterator<ITetromino>> bagSupplier){
        this.bagSupplier = bagSupplier;
    }

    @Override
    public void draw(GraphicsContext context, double x, double y) {
        context.drawImage(SpriteManager.NEXT_SPRITE, x, y);
        Iterator<ITetromino> bag = this.bagSupplier.get();
        for (int i = 0; i < BAG_SIZE; ++i){
            new TetrominoIconDrawer(bag.next())
                .draw(context, x, y + i * BASE * 3 + 4);
        }

    }

    @Override
    public double getWidth() {
        return TetrominoIconDrawer.WIDTH;
    }

    @Override
    public double getHeight() {
        return TetrominoIconDrawer.HEIGHT * BAG_SIZE;
    }

}
