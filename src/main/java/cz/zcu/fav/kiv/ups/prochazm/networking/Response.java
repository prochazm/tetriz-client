package cz.zcu.fav.kiv.ups.prochazm.networking;

import java.util.*;

public class Response {

    private static final String DELIM = " ";

    private final boolean success;
    private final List<String> stringTokens;
    private final List<Long> longTokens;

    Response(String message) {
        this.stringTokens = new ArrayList<>();
        this.longTokens = new ArrayList<>();
        this.success = message.charAt(0) == 'S';

        String data = message.substring(1);
        StringTokenizer tokenizer = new StringTokenizer(data, DELIM);

        while (tokenizer.hasMoreTokens()) {
            String token = tokenizer.nextToken();
            try {
                this.longTokens.add(Long.parseLong(token));
            } catch (NumberFormatException e){
                this.stringTokens.add(token);
            }
        }
    }

    public String getStringToken(int i){
        return this.stringTokens.get(i);
    }

    public long getLongToken(int i) {
        return this.longTokens.get(i);
    }

    public List<String> getStringTokens(){
        return Collections.unmodifiableList(this.stringTokens);
    }

    public List<Long> getLongTokens(){
        return Collections.unmodifiableList(this.longTokens);
    }

    public boolean getSuccess() {
        return this.success;
    }

}
