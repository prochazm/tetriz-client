package cz.zcu.fav.kiv.ups.prochazm.graphics;

import cz.zcu.fav.kiv.ups.prochazm.tetris.IGame;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;

public class ScoreDrawer implements IDrawer {

    private static final double WIDTH = BASE * 5;
    private static final double HEIGHT = BASE * 3;
    private static final double HALF_WIDTH = WIDTH / 2;
    private static final double HALF_HEIGHT = HEIGHT / 2;

    private final IGame game;

    public ScoreDrawer(IGame game){
        this.game = game;
    }

    @Override
    public void draw(GraphicsContext context, double x, double y) {
        context.save();

        context.drawImage(SpriteManager.SCORE_SPRITE, x, y);
        context.setFill(Color.BLACK);
        context.setFont(FONT_MONOSPACE);
        context.setTextBaseline(VPos.CENTER);
        context.setTextAlign(TextAlignment.CENTER);
        context.fillText(
            String.format("%d", this.game.getRowsCleared()),
            x + HALF_WIDTH, y + HALF_HEIGHT + 4);

        context.restore();
    }

    @Override
    public double getWidth() {
        return WIDTH;
    }

    @Override
    public double getHeight() {
        return HEIGHT;
    }
}
