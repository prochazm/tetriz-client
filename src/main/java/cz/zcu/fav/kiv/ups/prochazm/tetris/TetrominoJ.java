package cz.zcu.fav.kiv.ups.prochazm.tetris;

public class TetrominoJ extends Tetromino {

    private final Type type = Type.J;
    private boolean[][] boundingBox = {
        {true , false, false},
        {true , true , true },
        {false, false, false}
    };


    @Override
    public Type getType() {
        return type;
    }

    @Override
    boolean[][] getBoundingBox() {
        return this.boundingBox;
    }

    @Override
    void setBoundingBox(boolean[][] boundingBox) {
        this.boundingBox = boundingBox;
    }
}
