package cz.zcu.fav.kiv.ups.prochazm.tetris;

@FunctionalInterface
public interface TickListener {
    void onTick();
}
