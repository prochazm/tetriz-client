package cz.zcu.fav.kiv.ups.prochazm.tetris;

public abstract class Tetromino implements ITetromino {

    private static final int INITIAL_X = 0;
    private static final int INITIAL_Y = 0;

    private Rotation rotation = Rotation.BASE;
    private boolean[][] prevState;
    private Coordinate coordinate;

    Tetromino(){
        this.coordinate = new Coordinate(INITIAL_X, INITIAL_Y);
    }

    abstract boolean[][] getBoundingBox();

    abstract void setBoundingBox(boolean[][] boundingBox);


    public int getX() {
        return this.coordinate.x;
    }

    public int getY() {
        return this.coordinate.y;
    }

    public int getSize(){
        return this.getBoundingBox().length;
    }

    public int getBottomOffset(){
        return Offset.get(this, Offset.Side.BOTTOM);
    }

    public int getTopOffset(){
        return Offset.get(this, Offset.Side.TOP);
    }

    public int getWidth() {
       return getBoundingBox()[0].length;
    }

    public int getHeight() {
        return getBoundingBox().length;
    }

    public boolean getBlockAt(int x, int y){
        return getBoundingBox()[y][x];
    }

    int getRightOffset(){
        return Offset.get(this, Offset.Side.RIGHT);
    }

    int getLeftOffset(){
        return Offset.get(this, Offset.Side.LEFT);
    }

    public Rotation getRotation(){
        return this.rotation;
    }


    void translate(Direction direction){
        this.coordinate = this.coordinate.translated(direction);
    }

    void translate(int x, int y){
        this.coordinate = coordinate.translated(x, y);
    }

    void translateDown(int magnitude) {
        this.coordinate = this.coordinate.translated(magnitude);
    }

    void rotate(){
        boolean[][] boundingBox = this.getBoundingBox();
        int size = boundingBox.length;
        boolean[][] rotated = new boolean[size][size];

        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                rotated[i][j] = boundingBox[size - j - 1][i];
            }
        }

        this.rotation = this.rotation.nextClockwise();
        this.prevState = boundingBox;
        setBoundingBox(rotated);
    }

    void undoRotation(){
        setBoundingBox(this.prevState);
        this.rotation = this.rotation.nextAnticlockwise();
    }

    boolean isPipe(){
        return this.getType() == Type.I;
    }

    public enum Type {
        VOID, I, J, L, O, S, Z, T, GHOST;

        Tetromino getTetromino(){
            switch (this.ordinal()){
                case 1: return new TetrominoI();
                case 2: return new TetrominoJ();
                case 3: return new TetrominoL();
                case 4: return new TetrominoO();
                case 5: return new TetrominoS();
                case 6: return new TetrominoZ();
                case 7: return new TetrominoT();
                default: return null;
            }
        }
    }
}
