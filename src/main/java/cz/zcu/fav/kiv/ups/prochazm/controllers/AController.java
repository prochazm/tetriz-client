package cz.zcu.fav.kiv.ups.prochazm.controllers;

import cz.zcu.fav.kiv.ups.prochazm.App;
import cz.zcu.fav.kiv.ups.prochazm.model.Model;
import cz.zcu.fav.kiv.ups.prochazm.networking.Connection;
import javafx.beans.value.ChangeListener;

import java.util.Objects;

public abstract class AController {

    public static final String MENU_CONTROLLER = "menu";
    public static final String LOBBY_CONTROLLER = "lobby";
    public static final String GAME_CONTROLLER = "game";

    final Model model;
    private final ChangeListener<Connection.ConnectionState> disconnectHandler;

    AController(Model model) {
        this.model = model;
        this.disconnectHandler = (s, o, n) -> {
            if (n == Connection.ConnectionState.DISCONNECTED) {
                redirect(MENU_CONTROLLER);
            }
        };

        Connection connection = this.model.getProtocol().getConnection();
        if (Objects.nonNull(connection)) {
            connection.addDisconnectListener(this.disconnectHandler);
        }
    }

    public void destroy() {
        Connection connection = this.model.getProtocol().getConnection();
        if (Objects.nonNull(connection)){
            connection.removeDisconnectListener(this.disconnectHandler);
        }
    } // default do-nothing

    void redirect(String redirect, Object... redirectionArgs) {
        destroy();
        App.setRoot(redirect, redirectionArgs);
    }
}

