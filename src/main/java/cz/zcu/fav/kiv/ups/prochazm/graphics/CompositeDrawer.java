package cz.zcu.fav.kiv.ups.prochazm.graphics;

import javafx.scene.canvas.GraphicsContext;

import java.util.ArrayList;
import java.util.List;

public class CompositeDrawer implements IDrawer {

    private final List<IDrawer> drawers;
    private final List<Double> tx;
    private final List<Double> ty;

    private double width = 0.;
    private double height = 0.;

    public CompositeDrawer(){
       this.drawers = new ArrayList<>();
       this.tx = new ArrayList<>();
       this.ty = new ArrayList<>();
    }

    public CompositeDrawer include(IDrawer drawer, double x, double y){
        this.drawers.add(drawer);
        this.tx.add(x);
        this.ty.add(y);

        recalculateDimension();

        return this;
    }

    private void recalculateDimension() {
        double maxX = drawers.get(0).getWidth() + tx.get(0);
        double maxY = drawers.get(0).getHeight() + ty.get(0);

        for (int i = 1; i < drawers.size(); ++i){
            double candidateX = drawers.get(i).getWidth() + tx.get(i);
            if (candidateX > maxX) {
                maxX = candidateX;
            }
            double candidateY = drawers.get(i).getHeight() + ty.get(i);
            if (candidateY > maxY) {
                maxY = candidateY;
            }
        }

        this.width = maxX - tx.stream().min(Double::compareTo).orElse(0.);
        this.height = maxY - ty.stream().min(Double::compareTo).orElse(0.);
    }

    @Override
    public void draw(GraphicsContext context, double x, double y) {
        for (int i = 0; i < this.drawers.size(); ++i){
            this.drawers.get(i).draw(context, x + this.tx.get(i), y + this.ty.get(i));
        }
    }

    @Override
    public double getWidth() {
       return this.width;
    }

    @Override
    public double getHeight() {
       return this.height;
    }
}
