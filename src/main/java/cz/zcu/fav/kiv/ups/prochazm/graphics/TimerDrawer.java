package cz.zcu.fav.kiv.ups.prochazm.graphics;

import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;

import java.time.Duration;
import java.util.function.Supplier;

public class TimerDrawer extends DurationPrinter implements IDrawer {
    private static final double WIDTH;
    private static final double HEIGHT;

    static {
        Text t = new Text("00:00:00");
        t.setFont(FONT_MONOSPACE);
        WIDTH = t.getLayoutBounds().getWidth();
        HEIGHT = t.getLayoutBounds().getHeight();
    }

    private final Supplier<Integer> countdownSupplier;
    private boolean printTime = false;

    public TimerDrawer(Supplier<Long> timeSupplier, Supplier<Integer> countdownSupplier){
        super(() -> Duration.ofNanos(timeSupplier.get()));
        this.countdownSupplier = countdownSupplier;
    }

    @Override
    public void draw(GraphicsContext context, double x, double y) {
        context.save();
        context.setTextBaseline(VPos.TOP);
        context.setTextAlign(TextAlignment.CENTER);
        context.setFont(FONT_MONOSPACE);

        if (!this.printTime) {
            int cd = countdownSupplier.get();
            context.fillText(Integer.toString(cd), x, y);
            if (cd <= 0) {
                this.printTime = true;
            }
        } else {
            context.fillText(getTimestamp(), x, y);
        }

        context.restore();
    }

    @Override
    public double getWidth() {
        return WIDTH;
    }

    @Override
    public double getHeight() {
        return HEIGHT;
    }
}
