package cz.zcu.fav.kiv.ups.prochazm.networking;

import cz.zcu.fav.kiv.ups.prochazm.model.Move;
import cz.zcu.fav.kiv.ups.prochazm.model.beans.GameBean;
import cz.zcu.fav.kiv.ups.prochazm.model.beans.PlayerBean;
import cz.zcu.fav.kiv.ups.prochazm.model.beans.RoomBean;
import cz.zcu.fav.kiv.ups.prochazm.networking.ResponseValidator.TokenArity;
import cz.zcu.fav.kiv.ups.prochazm.tetris.GameAction;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;

public class Protocol {

    private Connection connection;

    private final static Map<String, GameAction> responseToGameAction;
    static {
        responseToGameAction = new HashMap<>();
        responseToGameAction.put("L", GameAction.MOVE_LEFT);
        responseToGameAction.put("R", GameAction.MOVE_RIGHT);
        responseToGameAction.put("D", GameAction.MOVE_DOWN);
        responseToGameAction.put("ROT", GameAction.ROTATE);
        responseToGameAction.put("SWP", GameAction.SWAP);
        responseToGameAction.put("DRP", GameAction.DROP);
    }

    public void setServer(String address, int port) {
        this.connection = new Connection(address, port);
    }

    public void unsetServer() {
        this.connection.close();
    }

    public Connection getConnection() {
        return this.connection;
    }

    /**
     * Performs login with passed user name. If
     * login attempt succeeds true is returned.
     *
     * @param name user name
     * @return true on success, false otherwise
     */
    public boolean login(String name) {
        Request loginRequest = Request.loginRequest(name);
        this.connection.setResumeRequest(loginRequest);

        return ResponseValidator
            .forResponse(this.connection.send(loginRequest))
            .tryMatch(TokenArity.NONE, TokenArity.NONE)
            .onMismatch(response -> unsetServer())
            .isMatchAndSuccess();
    }

    /**
     * Queries server for room identifiers and feeds
     * them to passed Consumer. Returns true if server
     * responds with success and int list of non-zero
     * length.
     *
     * @param idConsumer accepts room ids
     */
    public void queryRoomsIds(Consumer<Long> idConsumer) {
        ResponseValidator
            .forResponse(this.connection.send(Request.roomIdsQueryRequest()))
            .tryMatch(TokenArity.ANY, TokenArity.NONE)
            .ifMatchesRunOnLongs(true, idConsumer)
            .onMismatch(response -> unsetServer());
    }

    /**
     * Queries server for specific room represented by
     * passed roomBean. If call succeeds then roomBean
     * is updated and true returned.
     *
     * @param roomBean RoomBean to be updated
     * @return true if update succeeds
     */
    public boolean queryRoom(RoomBean roomBean) {
        return ResponseValidator
            .forResponse(this.connection.send(Request.roomQueryRequest(roomBean.getId())))
            .tryMatch(TokenArity.TWO, TokenArity.ONE)
            .ifMatchesRun(true, response -> {
                roomBean.setPlayerCount(response.getLongToken(0));
                roomBean.setPlayerLimit(response.getLongToken(1));
                roomBean.setCreator(response.getStringToken(0));
            })
            .onMismatch(response -> unsetServer())
            .isMatchAndSuccess();
    }


    public void queryPlayersInGame(GameBean gameBean, String localName) {
        ResponseValidator
            .forResponse(this.connection.send(Request.roomPlayersRequest(gameBean.getRoomId())))
            .tryMatch(TokenArity.NONE, TokenArity.MANY)
            .ifMatchesRunOnStrings(true, s -> {
                PlayerBean p = new PlayerBean(s);
                gameBean.getPlayers().add(p);
                if (p.getName().equals(localName)){
                    gameBean.setLocalPlayer(p);
                }
            })
            .onMismatch(response -> unsetServer());
    }

    public void queryStartDT(GameBean gameBean) {
        ResponseValidator
            .forResponse(this.connection.send(Request.gameStartDTRequest()))
            .tryMatch(TokenArity.ONE, TokenArity.NONE)
            .ifMatchesRunOnLongs(true, l -> gameBean.setStartTime(System.currentTimeMillis() + l))
            .onMismatch(response -> unsetServer());
    }

    /**
     * Sends a "join room" request for room specified
     * by passed RoomBean instance and returns true
     * if server responds positively to this request.
     *
     * @param roomBean Room to be joined
     * @return true on success
     */
    public boolean joinRoom(RoomBean roomBean) {
        return ResponseValidator
            .forResponse(this.connection.send(Request.roomJoinRequest(roomBean.getId())))
            .tryMatch(TokenArity.NONE, TokenArity.NONE)
            .isMatchAndSuccess();
    }

    /**
     * Sends a "create room" request for room specified
     * by passed RoomBean instance and returns true if
     * "create request" was accepted by the server.
     *
     * Also if room was successfully created then the
     * roomBean is updated to match with its state.
     *
     * @param roomBean room to be created
     * @return true on success
     */
    public boolean createRoom(RoomBean roomBean) {
        return ResponseValidator
            .forResponse(this.connection.send(Request.roomCreateRequest(roomBean.getPlayerLimit())))
            .tryMatch(TokenArity.ONE, TokenArity.NONE)
            .ifMatchesRunOnLongs(true, l -> {
                roomBean.setPlayerCount(0);
                roomBean.setId(l);
            })
            .isMatchAndSuccess();
    }

    public List<Move> queryPlayerMove(String playerName, long index) {
        List<Move> moves = new ArrayList<>();

        ResponseValidator
            .forResponse(this.connection.send(Request.readMoveRequest(playerName, index)))
            .tryMatch(TokenArity.MANY)
            .ifMatchesRunOnTokens(
                true,
                (timestamp, action) -> moves.add(new Move(responseToGameAction.get(action), timestamp))
            );

        return moves;
    }

    public void sendMove(GameAction move) {
        if (move == GameAction.NONE) {
            return; // server does not need to know that player did not do anything
        }

        while ( // be persistent
            !ResponseValidator
                .forResponse(this.connection.send(Request.makeMoveRequest(move)))
                .tryMatch(TokenArity.NONE)
                .onMismatch(response -> unsetServer())
                .isMatchAndSuccess()
        ){
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public boolean leaveRoom() {
        return ResponseValidator
            .forResponse(this.connection.send(Request.leaveRoomRequest()))
            .tryMatch(TokenArity.NONE)
            .onMismatch(response -> unsetServer())
            .isMatchAndSuccess();
    }

    public long getMyRoomId() {
        return ResponseValidator
            .forResponse(this.connection.send(Request.roomMyIdRequest()))
            .tryMatch(TokenArity.ONE, TokenArity.NONE)
            .onMismatch(response -> unsetServer())
            .ifMatchesGetLong(true, 0);
    }

    public long getRoomTime() {
        return ResponseValidator
            .forResponse(this.connection.send(Request.roomTimeRequest()))
            .tryMatch(TokenArity.ONE, TokenArity.NONE)
            .onMismatch(response -> unsetServer())
            .ifMatchesGetLong(true, 0);
    }

    public void querySeed(GameBean gameBean) {
        ResponseValidator
            .forResponse(this.connection.send(Request.seedRequest()))
            .tryMatch(TokenArity.ONE, TokenArity.NONE)
            .onMismatch(response -> unsetServer())
            .ifMatchesRunOnLongs(true, gameBean::setSeed);
    }
}
