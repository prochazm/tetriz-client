package cz.zcu.fav.kiv.ups.prochazm.graphics;

import cz.zcu.fav.kiv.ups.prochazm.tetris.ITetromino;
import cz.zcu.fav.kiv.ups.prochazm.tetris.Tetromino.Type;
import javafx.scene.image.Image;

import java.util.EnumMap;
import java.util.Map;

import static cz.zcu.fav.kiv.ups.prochazm.tetris.Tetromino.Type.*;

class SpriteManager {

    private static final Map<Type, Image> spriteMapping = new EnumMap<>(Type.class);
    private static final Map<Type, Image> spriteMappingGhost = new EnumMap<>(Type.class);

    public static final Image SCORE_SPRITE = loadFromResources("score");
    public static final Image HOLD_SPRITE = loadFromResources("hold");
    public static final Image NEXT_SPRITE = loadFromResources("next");

    static {
        // sprite init
        spriteMapping.put(VOID, loadFromResources("void"));
        spriteMapping.put(I, loadFromResources("cyan"));
        spriteMapping.put(J, loadFromResources("blue"));
        spriteMapping.put(L, loadFromResources("orange"));
        spriteMapping.put(O, loadFromResources("yellow"));
        spriteMapping.put(S, loadFromResources("green"));
        spriteMapping.put(Z, loadFromResources("purple"));
        spriteMapping.put(T, loadFromResources("red"));
        spriteMapping.put(GHOST, loadFromResources("black"));


        spriteMappingGhost.put(I, loadFromResources("ghostCyan"));
        spriteMappingGhost.put(J, loadFromResources("ghostBlue"));
        spriteMappingGhost.put(L, loadFromResources("ghostOrange"));
        spriteMappingGhost.put(O, loadFromResources("ghostYellow"));
        spriteMappingGhost.put(S, loadFromResources("ghostGreen"));
        spriteMappingGhost.put(Z, loadFromResources("ghostPurple"));
        spriteMappingGhost.put(T, loadFromResources("ghostRed"));
    }

    private static Image loadFromResources(String name){
        return new Image(SpriteManager.class.getResourceAsStream(name + ".png"));
    }

    public static Image spriteOfGhost(ITetromino tetromino){
        return spriteOfGhost(tetromino.getType());
    }

    private static Image spriteOfGhost(Type tetromino){
        return spriteMappingGhost.get(tetromino);
    }

    public static Image spriteOf(ITetromino tetromino) {
        return spriteOf(tetromino.getType());
    }

    public static Image spriteOf(Type tetromino) {
        return spriteMapping.get(tetromino);
    }

}
