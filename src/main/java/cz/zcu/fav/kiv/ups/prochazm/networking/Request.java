package cz.zcu.fav.kiv.ups.prochazm.networking;

import cz.zcu.fav.kiv.ups.prochazm.tetris.GameAction;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;

public class Request {

    private static final String LOGIN = "L";
    private static final String ROOM_COUNT_QUERY = "C";
    private static final String ROOM_QUERY = "R";
    private static final String JOIN_ROOM = "J";
    private static final String LEAVE_ROOM = "V";
    private static final String NEW_ROOM = "N";
    private static final String PLAYERS_IN_ROOM = "P";
    private static final String DELTA_T_REQUEST = "T";
    private static final String MY_ROOM = "?";
    private static final String MOVE = "M";
    private static final String GET_MOVE = "G";
    private static final String ROOM_TIME = "O";
    private static final String SEED = "S";

    private static final String NO_DATA = "";

    private static final HashMap<GameAction, String> gameActionToQuery;
    static {
        gameActionToQuery = new HashMap<>();
        gameActionToQuery.put(GameAction.MOVE_LEFT, "L");
        gameActionToQuery.put(GameAction.MOVE_DOWN, "D");
        gameActionToQuery.put(GameAction.MOVE_RIGHT, "R");
        gameActionToQuery.put(GameAction.DROP, "DRP");
        gameActionToQuery.put(GameAction.ROTATE, "ROT");
        gameActionToQuery.put(GameAction.SWAP, "SWP");
    }

    private final byte[] message;


    private Request(String command) {
       this.message = command
           .concat("\n")
           .getBytes(StandardCharsets.US_ASCII);
    }

    private Request(String command, String args){
        this.message =
            command
            .concat(args)
            .concat("\n")
            .getBytes(StandardCharsets.US_ASCII);
    }

    private Request(String command, long args){
        this.message =
            command
            .concat(Long.toString(args))
            .concat("\n")
            .getBytes(StandardCharsets.US_ASCII);
    }

    public byte[] getBytes(){
        return message;
    }

    public static Request loginRequest(String name){
        return new Request(LOGIN, name);
    }

    public static Request roomIdsQueryRequest(){
        return new Request(ROOM_COUNT_QUERY, NO_DATA);
    }

    public static Request roomQueryRequest(long roomId){
        return new Request(ROOM_QUERY, roomId);
    }

    public static Request roomJoinRequest(long roomId){
        return new Request(JOIN_ROOM, roomId);
    }

    public static Request roomCreateRequest(long playerCount) {
        return new Request(NEW_ROOM, playerCount);
    }

    public static Request roomPlayersRequest(long roomId) {
        return new Request(PLAYERS_IN_ROOM, roomId);
    }

    public static Request gameStartDTRequest() {
        return new Request(DELTA_T_REQUEST);
    }

    public static Request readMoveRequest(String playerName, long index) {
        return new Request(String.format("%s %s %d", GET_MOVE, playerName, index));
    }

    public static Request makeMoveRequest(GameAction gameAction) {
        return new Request(MOVE, gameActionToQuery.get(gameAction));
    }

    public static Request leaveRoomRequest() {
        return new Request(LEAVE_ROOM);
    }

    public static Request roomMyIdRequest() {
        return new Request(MY_ROOM);
    }

    public static Request roomTimeRequest() {
        return new Request(ROOM_TIME);
    }

    public static Request seedRequest() {
        return new Request(SEED);
    }
}
