package cz.zcu.fav.kiv.ups.prochazm.engine;

import cz.zcu.fav.kiv.ups.prochazm.model.IGameManager;
import cz.zcu.fav.kiv.ups.prochazm.model.Move;
import javafx.beans.value.ChangeListener;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
import java.util.function.Consumer;

public class InputSource extends TimerTask {

    private final IGameManager gameManager;
    private final String playerName;
    private final List<ChangeListener<List<Move>>> moveListeners;
    private final List<Consumer<Long>> lastResponseConsumer;

    private long lastResponseTime;
    private List<Move> actions;
    private long index;


    public InputSource(String playerName, IGameManager gameManager){
        this.actions = new ArrayList<>();
        this.gameManager = gameManager;
        this.playerName = playerName;
        this.moveListeners = new ArrayList<>();
        this.lastResponseTime = System.currentTimeMillis();
        this.lastResponseConsumer = new ArrayList<>();
    }


    @Override
    public void run() {
        List<Move> newActions = this.gameManager.queryGameAction(this.playerName, this.index);
        if (newActions != null) {
            this.index += newActions.size();
            this.moveListeners.forEach(a -> a.changed(null, this.actions, newActions));
            this.actions = newActions;
            if (newActions.size() > 0){
                this.lastResponseTime = System.currentTimeMillis();
            }
            this.lastResponseConsumer.forEach(c -> c.accept(this.lastResponseTime));
        }
    }

    public void addMoveListener(ChangeListener<List<Move>> listener) {
        this.moveListeners.add(listener);
    }

    public void addLastResponseConsumer(Consumer<Long> consumer) {
        this.lastResponseConsumer.add(consumer);
    }
}
