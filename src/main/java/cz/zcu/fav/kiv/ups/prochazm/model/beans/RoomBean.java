package cz.zcu.fav.kiv.ups.prochazm.model.beans;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleStringProperty;

public class RoomBean {

    private final SimpleLongProperty id;
    private final SimpleLongProperty playerCount;
    private final SimpleLongProperty playerLimit;
    private final SimpleStringProperty creator;


    public RoomBean() {
        this(0, 0, 0, "");
    }

    public RoomBean(long id) {
        this(id, 0, 0, "");
    }

    public RoomBean(long id, long count, long countLimit, String creator){
        this.id = new SimpleLongProperty(id);
        this.playerCount = new SimpleLongProperty(count);
        this.playerLimit = new SimpleLongProperty(countLimit);
        this.creator = new SimpleStringProperty(creator);
    }

    public long getId() {
        return id.get();
    }

    public SimpleLongProperty idProperty() {
        return id;
    }

    public void setId(long id) {
        this.id.set(id);
    }

    public long getPlayerCount() {
        return playerCount.get();
    }

    public SimpleLongProperty playerCountProperty() {
        return playerCount;
    }

    public void setPlayerCount(long playerCount) {
        this.playerCount.set(playerCount);
    }

    public long getPlayerLimit() {
        return playerLimit.get();
    }

    public SimpleLongProperty playerLimitProperty() {
        return playerLimit;
    }

    public void setPlayerLimit(long playerLimit) {
        this.playerLimit.set(playerLimit);
    }

    public String getCreator() {
        return creator.get();
    }

    public SimpleStringProperty creatorProperty() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator.set(creator);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof RoomBean)) return false;
        RoomBean roomBean = (RoomBean) o;
        return getId() == roomBean.getId();
    }

    @Override
    public int hashCode() {
        return (int)getId();
    }
}
