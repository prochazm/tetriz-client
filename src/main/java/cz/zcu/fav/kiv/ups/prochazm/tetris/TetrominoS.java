package cz.zcu.fav.kiv.ups.prochazm.tetris;

public class TetrominoS extends Tetromino {

    private final Type type = Type.S;
    private boolean[][] boundingBox = {
        {false, true , true },
        {true , true , false},
        {false, false, false}
    };


    @Override
    public Type getType() {
        return type;
    }

    @Override
    boolean[][] getBoundingBox() {
        return this.boundingBox;
    }

    @Override
    void setBoundingBox(boolean[][] boundingBox) {
        this.boundingBox = boundingBox;
    }
}
