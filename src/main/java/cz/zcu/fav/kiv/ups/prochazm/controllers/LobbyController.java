package cz.zcu.fav.kiv.ups.prochazm.controllers;

import cz.zcu.fav.kiv.ups.prochazm.model.LobbyManager;
import cz.zcu.fav.kiv.ups.prochazm.model.Model;
import cz.zcu.fav.kiv.ups.prochazm.model.beans.RoomBean;
import cz.zcu.fav.kiv.ups.prochazm.networking.Connection;
import javafx.application.Platform;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.css.PseudoClass;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.util.Callback;

import java.util.Objects;
import java.util.Timer;
import java.util.TimerTask;

public class LobbyController extends AController {

    private final SimpleBooleanProperty roomIsReady;
    private final SimpleObjectProperty<RoomBean> registeredRoom;
    private final ChangeListener<Connection.ConnectionState> disconnectListener;
    private ChangeListener<Connection.ConnectionState> interruptListener;

    private final LobbyManager lobbyManager;
    private final Timer refreshTimer;

    @FXML
    private TableView<RoomBean> lobbyTable;

    @FXML
    private TableColumn<RoomBean, Number> roomColumn;

    @FXML
    private TableColumn<RoomBean, String> ownerColumn;

    @FXML
    private TableColumn<RoomBean, Number> playerCountColumn;

    @FXML
    private TableColumn<RoomBean, Number> playerCountLimitColumn;


    @FXML
    private Button joinBtn;

    @FXML
    private Button createBtn;

    @FXML
    private Button leaveBtn;

    @FXML
    private Button refreshBtn;

    @FXML
    private Slider playerCountSlider;


    public LobbyController(Model model) {
        super(model);
        this.lobbyManager = new LobbyManager(model.getProtocol());
        this.registeredRoom = new SimpleObjectProperty<>();
        this.roomIsReady = new SimpleBooleanProperty();
        this.refreshTimer = new Timer();
        this.disconnectListener = (s, o, n) -> {
            if (n != Connection.ConnectionState.CONNECTED) {
                super.redirect(MENU_CONTROLLER);
            }
        };

    }

    @FXML
    private void initialize() {
        // setup cell values for the table
        this.roomColumn.setCellValueFactory(e -> e.getValue().idProperty());
        this.ownerColumn.setCellValueFactory(e -> e.getValue().creatorProperty());
        this.playerCountColumn.setCellValueFactory(e -> e.getValue().playerCountProperty());
        this.playerCountLimitColumn.setCellValueFactory(e -> e.getValue().playerLimitProperty());
        this.lobbyTable.setItems(this.lobbyManager.getRoomBeans());

        // and add highlight for joined room
        Platform.runLater(() -> {
            final PseudoClass joinedPseudoClass = PseudoClass.getPseudoClass("joined");
            this.lobbyTable.setRowFactory(new Callback<>() {
                @Override
                public TableRow<RoomBean> call(TableView<RoomBean> param) {
                    return new TableRow<>() {
                        @Override
                        protected void updateItem(RoomBean roomBean, boolean empty) {
                            super.updateItem(roomBean, empty);
                            boolean joined = !isSelected() && !empty && roomBean == registeredRoom.get();
                            pseudoClassStateChanged(joinedPseudoClass, joined);
                        }
                    };
                }
            });
            registerRoom(this.lobbyManager.getMyRoom());
        });


        // joinBtn disabled if selection is empty or already waiting for game
        this.joinBtn.disableProperty().bind(this.lobbyTable
            .getSelectionModel()
            .selectedItemProperty()
            .isNull()
            .or(this.registeredRoom.isNotNull())
        );
        this.leaveBtn.disableProperty().bind(this.registeredRoom.isNull());
        this.createBtn.disableProperty().bind(this.registeredRoom.isNotNull());


        this.roomIsReady.addListener((s, o, n) -> redirect(GAME_CONTROLLER, this.registeredRoom.get().getId()));
        super.model.getProtocol().getConnection().addDisconnectListener(this.disconnectListener);

        this.interruptListener = (s, o, n) -> Platform.runLater(
            () -> this.refreshBtn.setText(n == Connection.ConnectionState.CONNECTED ? "⟲" : "⟲ Disconnected!")
        );
        super.model.getProtocol().getConnection().addInterruptListener(this.interruptListener);


        this.refreshTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                lobbyManager.updateAll();
            }
        }, 0, 5000);
    }

    private void registerRoom(RoomBean rb){
        this.registeredRoom.set(rb);
        if (Objects.isNull(rb)) {
            return;
        }
        this.roomIsReady.bind( // waiting for a room and player count = player limit
            this.registeredRoom.get().playerCountProperty().isEqualTo(
                this.registeredRoom.get().playerLimitProperty()
            )
        );
    }

    @FXML
    private void joinSelected() {
        RoomBean target = this.lobbyTable.getSelectionModel().getSelectedItem();
        new Thread(() -> {
            if (this.lobbyManager.join(target)) {
                Platform.runLater(() -> registerRoom(target));
            }
            this.lobbyManager.updateAll();
        }).start();
    }

    @FXML
    private void createNew() {
        new Thread(() -> {
            RoomBean roomBean = this.lobbyManager
                .create((int)this.playerCountSlider.getValue(), this.model.getName());
            Platform.runLater(() -> {
                registerRoom(roomBean);
                refreshTable();
            });
        }).start();
    }

    @FXML
    private void refreshTable() {
        new Thread(this.lobbyManager::updateAll).start();
    }

    @FXML
    private void leaveRoom() {
        new Thread(() -> {
            if (this.lobbyManager.leave(this.registeredRoom.get())) {
                Platform.runLater(() -> {
                    this.registeredRoom.set(null);
                    refreshTable();
                });
            }
        }).start();

    }

    @FXML
    private void exitLobby() {
        this.model.getProtocol().unsetServer();
    }

    @Override
    public void destroy() {
        super.destroy();
        this.refreshTimer.cancel();
        this.model.getProtocol().getConnection().removeDisconnectListener(this.disconnectListener);
        this.model.getProtocol().getConnection().removeInterruptListener(this.interruptListener);
    }
}
