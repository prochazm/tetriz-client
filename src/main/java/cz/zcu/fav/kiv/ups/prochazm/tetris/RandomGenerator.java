package cz.zcu.fav.kiv.ups.prochazm.tetris;

import java.util.*;
import java.util.function.Consumer;

/**
 * Class RandomGenerator
 *
 * This is implementation of so called "Random Generator"
 * or "Random Bag". This is infinite iterable collection
 * of tetrominoes which has guaranteed max distance between
 * same class tetrominoes (i.e. 12, min is 2).
 *
 * This is achieved by chaining permutations of 7-ary
 * tuple containing each tetromino.
 *
 * @since 11.10.2019
 * @author Martin Procházka
 */
class RandomGenerator implements Iterable<ITetromino>{

    private static final int MIN_SIZE = 7;

    private final LCG rng;
    private final Deque<ITetromino> tetrominoes;

    RandomGenerator(long seed){
        this.rng = new LCG(seed);
        this.tetrominoes = new ArrayDeque<>();
    }

    private void offerPermutation(){
        ITetromino[] base = {
            Tetromino.Type.I.getTetromino(),
            Tetromino.Type.J.getTetromino(),
            Tetromino.Type.L.getTetromino(),
            Tetromino.Type.O.getTetromino(),
            Tetromino.Type.S.getTetromino(),
            Tetromino.Type.Z.getTetromino(),
            Tetromino.Type.T.getTetromino(),
        };

        for (int i = 0; i < base.length; i++) {
            int j = this.rng.next();
            ITetromino tmp = base[i];
            base[i] = base[j];
            base[j] = tmp;
        }

        for (ITetromino tetromino : base) {
            this.tetrominoes.offer(tetromino);
        }
    }

    ITetromino poll(){
        if (this.tetrominoes.isEmpty()){
            offerPermutation();
        }

        return this.tetrominoes.poll();
    }

    @Override
    public Iterator<ITetromino> iterator() {
        if (this.tetrominoes.size() < MIN_SIZE){
            offerPermutation();
        }

        return this.tetrominoes.iterator();
    }

    @Override
    public void forEach(Consumer<? super ITetromino> action) {

        if (this.tetrominoes.size() < MIN_SIZE){
            offerPermutation();
        }

        Iterator<ITetromino> i = iterator();

        for (int j = 0; j < MIN_SIZE; ++j){
            action.accept(i.next());
        }
    }

    private static class LCG {

        private static final long MODULUS = 961;
        private static final long MULTIPLIER = 32;
        private static final long INCREMENT = 1031;

        private long result;

        LCG(long seed){
            this.result = seed;
        }

        int next(){
            this.result = Math.abs(MULTIPLIER * this.result + INCREMENT) % MODULUS;

            return (int)(this.result % 7);
        }
    }
}
