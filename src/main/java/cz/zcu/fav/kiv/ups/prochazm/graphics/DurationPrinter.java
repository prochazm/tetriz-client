package cz.zcu.fav.kiv.ups.prochazm.graphics;

import java.time.Duration;
import java.util.function.Supplier;

abstract class DurationPrinter {

    private final Supplier<Duration> supplier;
    private Duration snapshot;
    private String snapshotString;

    DurationPrinter(Supplier<Duration> durationSupplier) {
        this.supplier = durationSupplier;
    }

    String getTimestamp() {
        if (this.supplier.get().equals(this.snapshot)){
            return this.snapshotString;
        }
        this.snapshot = this.supplier.get();

        return this.snapshotString = String.format("%d:%02d:%02d",
            this.snapshot.toMinutes(),
            this.snapshot.toSecondsPart(),
            this.snapshot.toMillisPart() / 10
        );
    }
}
