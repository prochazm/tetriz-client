package cz.zcu.fav.kiv.ups.prochazm.model;

import cz.zcu.fav.kiv.ups.prochazm.tetris.GameAction;

public class Move {
    private final GameAction action;
    private final long timestamp;


    public Move(GameAction action, long timestamp) {
        this.action = action;
        this.timestamp = timestamp;
    }

    public GameAction getAction() {
        return this.action;
    }

    public long getTimestamp() {
        return this.timestamp;
    }
}
