package cz.zcu.fav.kiv.ups.prochazm.model;

import cz.zcu.fav.kiv.ups.prochazm.tetris.GameAction;
import cz.zcu.fav.kiv.ups.prochazm.model.beans.GameBean;
import cz.zcu.fav.kiv.ups.prochazm.networking.Protocol;

import java.util.List;

public class GameManager implements IGameManager {

    private final Protocol protocol;
    //todo could hold the managed game instead of it being in controller...

    public GameManager(Protocol protocol){
        this.protocol = protocol;
    }

    public List<Move> queryGameAction(String playerName, long index){
        return this.protocol.queryPlayerMove(playerName, index);
    }

    public void sendGameAction(GameAction gameAction) {
        this.protocol.sendMove(gameAction);
    }

    @Override
    public boolean leave() {
        return this.protocol.leaveRoom();
    }

    @Override
    public long getRoomTime() {
        return this.protocol.getRoomTime() * 1000000;
    }

    @Override
    public GameBean getGameForRoom(long roomId, String localName) {

        GameBean gameBean = new GameBean();
        gameBean.setRoomId(roomId);

        gameBean.getPlayers().clear();
        this.protocol.queryPlayersInGame(gameBean, localName);
        this.protocol.querySeed(gameBean);
        this.protocol.queryStartDT(gameBean);

        return gameBean;
    }
}
