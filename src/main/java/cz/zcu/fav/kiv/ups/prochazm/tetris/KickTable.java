package cz.zcu.fav.kiv.ups.prochazm.tetris;

class KickTable {
    public static final int KICK_COUNT = 5;

    private static final int[][][] tableOther = {
        {{ 0, 0}, {-1, 0}, {-1,-1}, { 0, 2}, {-1, 2}},
        {{ 0, 0}, { 1, 0}, { 1, 1}, { 0,-2}, { 1,-2}},
        {{ 0, 0}, { 1, 0}, { 1, 1}, { 0,-2}, { 1,-2}},
        {{ 0, 0}, {-1, 0}, {-1,-1}, { 0, 2}, {-1, 2}},
        {{ 0, 0}, { 1, 0}, { 1,-1}, { 0, 2}, { 1, 2}},
        {{ 0, 0}, {-1, 0}, {-1, 1}, { 0,-2}, {-1,-2}},
        {{ 0, 0}, {-1, 0}, {-1, 1}, { 0,-2}, {-1,-2}},
        {{ 0, 0}, { 1, 0}, { 1, 1}, { 0, 2}, { 1, 2}},
    };

    private static final int[][][] tableI = {
        {{ 0, 0}, {-2, 0}, { 1, 0}, {-2, 1}, { 1,-2}},
        {{ 0, 0}, { 2, 0}, {-1, 0}, { 2,-1}, {-1, 2}},
        {{ 0, 0}, {-1, 0}, { 2, 0}, {-1,-2}, { 2, 1}},
        {{ 0, 0}, { 1, 0}, {-2, 0}, { 1, 2}, {-2,-1}},
        {{ 0, 0}, { 2, 0}, {-1, 0}, { 2,-1}, {-1, 2}},
        {{ 0, 0}, {-2, 0}, { 1, 0}, {-2, 1}, { 1,-2}},
        {{ 0, 0}, { 1, 0}, {-2, 0}, { 1, 2}, {-2,-1}},
        {{ 0, 0}, {-1, 0}, { 2, 0}, {-1,-2}, { 2, 1}},
    };

    public static int getOffsetX(Tetromino t, int i){
        return t.isPipe()
            ? getOffsetRow(tableI, t.getRotation())[i][0]
            : getOffsetRow(tableOther, t.getRotation())[i][0];
    }

    public static int getOffsetY(Tetromino t, int i){
        return t.isPipe()
            ? getOffsetRow(tableI, t.getRotation())[i][1]
            : getOffsetRow(tableOther, t.getRotation())[i][1];
    }

    private static int[][] getOffsetRow(int[][][] table, Rotation rot){
        switch (rot){
            case BASE:
                return table[0];
            case RIGHT:
                return table[2];
            case DOUBLE:
                return table[4];
            case LEFT:
            default:
                return table[6];
        }
    }

}
