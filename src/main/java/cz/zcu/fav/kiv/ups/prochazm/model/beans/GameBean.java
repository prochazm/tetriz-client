package cz.zcu.fav.kiv.ups.prochazm.model.beans;

import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.function.Supplier;

public class GameBean {

    private final ObservableList<PlayerBean> players;
    private final SimpleObjectProperty<PlayerBean> localPlayer;
    private final SimpleLongProperty startTime;
    private final SimpleLongProperty roomId;
    private final SimpleLongProperty seed;

    private final Supplier<Boolean> started;
    private final Supplier<Boolean> finished;

    public GameBean() {
        this(0);
    }

    public GameBean(long roomId) {
        this.players = FXCollections.observableArrayList();
        this.localPlayer = new SimpleObjectProperty<>();
        this.startTime = new SimpleLongProperty(Long.MAX_VALUE);
        this.roomId = new SimpleLongProperty(roomId);
        this.seed = new SimpleLongProperty(0);

        this.started = () -> this.startTime.get() - System.currentTimeMillis() < 0;
        this.finished = () -> getPlayers().stream().allMatch(p -> p.getGame().getState().isDone());
    }

    public ObservableList<PlayerBean> getPlayers() {
        return players;
    }

    public PlayerBean getLocalPlayer() {
        return localPlayer.get();
    }

    public SimpleObjectProperty<PlayerBean> localPlayerProperty() {
        return localPlayer;
    }

    public void setLocalPlayer(PlayerBean localPlayer) {
        this.localPlayer.set(localPlayer);
    }

    public long getStartTime() {
        return startTime.get();
    }

    public SimpleLongProperty startTimeProperty() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime.set(startTime);
    }

    public long getRoomId() {
        return roomId.get();
    }

    public SimpleLongProperty roomIdProperty() {
        return roomId;
    }

    public void setRoomId(long roomId) {
        this.roomId.set(roomId);
    }

    public long getSeed() {
        return seed.get();
    }

    public SimpleLongProperty seedProperty() {
        return seed;
    }

    public void setSeed(long seed) {
        this.seed.set(seed);
    }

    public boolean isStarted() {
        return this.started.get();
    }

    public boolean isFinished() {
        return this.finished.get();
    }
}
