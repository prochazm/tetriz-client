package cz.zcu.fav.kiv.ups.prochazm.graphics;

import cz.zcu.fav.kiv.ups.prochazm.tetris.ITetromino;
import javafx.geometry.VPos;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.TextAlignment;

import java.util.function.Supplier;

public class HoldDrawer implements IDrawer {

    private final Supplier<ITetromino> holdSupplier;

    HoldDrawer(Supplier<ITetromino> holdSupplier) {
        this.holdSupplier = holdSupplier;
    }

    @Override
    public void draw(GraphicsContext context, double x, double y) {
        context.drawImage(SpriteManager.HOLD_SPRITE, x, y);
        ITetromino hold = this.holdSupplier.get();
        if (hold == null) {
            context.save();
            context.setTextAlign(TextAlignment.CENTER);
            context.setTextBaseline(VPos.CENTER);
            context.setFont(FONT_MONOSPACE_SMALLER);
            context.fillText(
                "EMPTY",
                x + SpriteManager.HOLD_SPRITE.getWidth() / 2,
                y + SpriteManager.HOLD_SPRITE.getHeight() / 2 + 4);
            context.restore();
        } else {
            new TetrominoIconDrawer(this.holdSupplier.get()).draw(context, x, y + 4);
        }
    }

    @Override
    public double getWidth() {
        return SpriteManager.HOLD_SPRITE.getWidth();
    }

    @Override
    public double getHeight() {
        return SpriteManager.HOLD_SPRITE.getHeight();
    }
}
