package cz.zcu.fav.kiv.ups.prochazm.tetris;

import java.time.Duration;
import java.util.Iterator;

public interface IGame {

    int BOARD_HEIGHT = 22;
    int BOARD_HEIGHT_HIDDEN = 2;
    int BOARD_HEIGHT_VISIBLE = 20;
    int BOARD_WIDTH = 10;


    Game.State getState();

    ITetromino getPiece();

    ITetromino getHold();

    Iterator<ITetromino> getTetrominoBag();

    Duration getTime();

    Tetromino.Type getTile(int x, int  y);

    void setTime(long ms);

    int getGhost();

    int getRowsCleared();

    void moveLeft();

    void moveRight();

    void moveDown();

    void drop();

    void swap();

    void rotate();

    void addTickListener(TickListener tickListener);

    void setAutoDrop(boolean autoDrop);

}
