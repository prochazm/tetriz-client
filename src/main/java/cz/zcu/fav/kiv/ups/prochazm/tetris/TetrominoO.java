package cz.zcu.fav.kiv.ups.prochazm.tetris;

public class TetrominoO extends Tetromino {

    private final Type type = Type.O;
    private boolean[][] boundingBox = {
        {true , true},
        {true , true},
    };


    @Override
    public Type getType() {
        return type;
    }

    @Override
    boolean[][] getBoundingBox() {
        return this.boundingBox;
    }

    @Override
    void setBoundingBox(boolean[][] boundingBox) {
        this.boundingBox = boundingBox;
    }
}
