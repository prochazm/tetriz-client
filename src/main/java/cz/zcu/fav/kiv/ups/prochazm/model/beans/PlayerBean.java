package cz.zcu.fav.kiv.ups.prochazm.model.beans;

import cz.zcu.fav.kiv.ups.prochazm.tetris.IGame;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleLongProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class PlayerBean {

    private final SimpleStringProperty name;
    private final SimpleLongProperty finishTime;
    private final SimpleObjectProperty<IGame> game;
    private final SimpleBooleanProperty active;

    public PlayerBean() {
        this("");
    }

    public PlayerBean(String name){
        this(name, true);
    }

    public PlayerBean(String name, boolean active) {
        this.name = new SimpleStringProperty(name);
        this.finishTime = new SimpleLongProperty(Long.MAX_VALUE);
        this.game = new SimpleObjectProperty<>();
        this.active = new SimpleBooleanProperty(active);
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.set(name);
    }

    public long getFinishTime() {
        return finishTime.get();
    }

    public SimpleLongProperty finishTimeProperty() {
        return finishTime;
    }

    public void setFinishTime(long finishTime) {
        this.finishTime.set(finishTime);
    }

    public IGame getGame() {
        return game.get();
    }

    public SimpleObjectProperty<IGame> gameProperty() {
        return game;
    }

    public void setGame(IGame game) {
        this.game.set(game);
    }

    public boolean isActive() {
        return active.get();
    }

    public SimpleBooleanProperty activeProperty() {
        return active;
    }

    public void setActive(boolean active) {
        this.active.set(active);
    }
}
