package cz.zcu.fav.kiv.ups.prochazm.graphics;

import cz.zcu.fav.kiv.ups.prochazm.model.beans.PlayerBean;
import javafx.scene.canvas.GraphicsContext;

public class GameDrawer implements IDrawer {

    private final BoardDrawer boardDrawer;
    private final BagDrawer bagDrawer;
    private final ScoreDrawer scoreDrawer;
    private final HoldDrawer holdDrawer;
    private final PlayerInfoDrawer playerInfoDrawer;

    public GameDrawer(PlayerBean playerBean){
        this.boardDrawer = new BoardDrawer(playerBean.getGame());
        this.bagDrawer = new BagDrawer(playerBean.getGame()::getTetrominoBag);
        this.scoreDrawer = new ScoreDrawer(playerBean.getGame());
        this.holdDrawer = new HoldDrawer(playerBean.getGame()::getHold);
        this.playerInfoDrawer = new PlayerInfoDrawer(playerBean);
    }


    @Override
    public void draw(GraphicsContext context, double x, double y) {
        this.boardDrawer.draw(context, x, y);
        this.bagDrawer.draw(context, x + 10.5 * BASE, y);
        this.scoreDrawer.draw(context, x + 10.5 * BASE, y + 17 * BASE);
        this.holdDrawer.draw(context, x + 10.5 * BASE, y + 13 * BASE);
        this.playerInfoDrawer.draw(context, x, y + 20 * BASE);
    }

    @Override
    public double getWidth() {
        return this.boardDrawer.getWidth() + this.bagDrawer.getWidth();
    }

    @Override
    public double getHeight() {
        return this.boardDrawer.getHeight() + this.playerInfoDrawer.getHeight();
    }
}
