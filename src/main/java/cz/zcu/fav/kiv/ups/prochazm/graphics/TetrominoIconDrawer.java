package cz.zcu.fav.kiv.ups.prochazm.graphics;

import cz.zcu.fav.kiv.ups.prochazm.tetris.ITetromino;
import javafx.scene.canvas.GraphicsContext;

public class TetrominoIconDrawer implements IDrawer {
    public static final double WIDTH = BASE * 5;
    public static final double HEIGHT = BASE * 3;

    private final ITetromino tetromino;

    TetrominoIconDrawer(ITetromino tetromino){
        this.tetromino = tetromino;
    }

    @Override
    public void draw(GraphicsContext context, double x, double y) {
        context.save();

        int size = this.tetromino.getSize();
        double leftOffset = x + (BASE * (5 - size)) / 2;
        double topOffset = y + (BASE * ((3 - size) + (this.tetromino.getBottomOffset() - this.tetromino.getTopOffset()))) / 2;

        for (int tileY = 0; tileY < size; tileY++) {
            for (int tileX = 0; tileX < size; tileX++) {
                if (this.tetromino.getBlockAt(tileX, tileY)) {
                    context.drawImage(
                        SpriteManager.spriteOf(tetromino.getType()),
                        leftOffset + tileX * BASE,
                        topOffset + tileY * BASE
                    );
                }
            }
        }

        context.restore();
    }

    @Override
    public double getWidth() {
        return WIDTH;
    }

    @Override
    public double getHeight() {
        return HEIGHT;
    }
}
