package cz.zcu.fav.kiv.ups.prochazm.tetris;

public interface ITetromino {

    Tetromino.Type getType();

    int getX();

    int getY();

    int getWidth();

    int getHeight();

    int getBottomOffset();

    int getTopOffset();

    int getSize();

    boolean getBlockAt(int x, int y);
}
