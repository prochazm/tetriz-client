package cz.zcu.fav.kiv.ups.prochazm.model;

import cz.zcu.fav.kiv.ups.prochazm.networking.Protocol;

public class Model {

    private final Protocol protocol;

    private String name;

    public Model() {
        this.protocol = new Protocol();
    }

    public Protocol getProtocol() {
        return protocol;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
