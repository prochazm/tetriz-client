package cz.zcu.fav.kiv.ups.prochazm.tetris;

class Coordinate {
    public final int x;
    public final int y;

    Coordinate(int x, int y){
        this.x = x;
        this.y = y;
    }

    Coordinate translated(Direction direction) {
        return new Coordinate(
            this.x + direction.dx,
            this.y + direction.dy
        );
    }

    Coordinate translated(int magnitude) {
        return new Coordinate(
            this.x + Direction.DOWN.dx * magnitude,
            this.y + Direction.DOWN.dy * magnitude
        );
    }

    Coordinate translated(int x, int y){
        return new Coordinate(this.x + x, this.y + y);
    }
}
