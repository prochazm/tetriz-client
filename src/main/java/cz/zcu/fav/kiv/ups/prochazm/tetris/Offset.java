package cz.zcu.fav.kiv.ups.prochazm.tetris;

class Offset {
    private static final int[][][] offsets = {
        //T  B  L  R
        {},                                                       // VOID
        {{1, 2, 0, 0}, {0, 0, 2, 1}, {2, 1, 0, 0}, {0, 0, 1, 2}}, // I
        {{0, 1, 0, 0}, {0, 0, 1, 0}, {1, 0, 0, 0}, {0, 0, 0, 1}}, // J
        {{0, 1, 0, 0}, {0, 0, 1, 0}, {1, 0, 0, 0}, {0, 0, 0, 1}}, // L
        {{0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}, {0, 0, 0, 0}}, // O
        {{0, 1, 0, 0}, {0, 0, 1, 0}, {1, 0, 0, 0}, {0, 0, 0, 1}}, // S
        {{0, 1, 0, 0}, {0, 0, 1, 0}, {1, 0, 0, 0}, {0, 0, 0, 1}}, // Z
        {{0, 1, 0, 0}, {0, 0, 1, 0}, {1, 0, 0, 0}, {0, 0, 0, 1}}  // T
    };

    static int get(Tetromino t, Side s){
        return offsets[t.getType().ordinal()][t.getRotation().ordinal()][s.ordinal()];
    }

    enum Side{
        TOP, BOTTOM, LEFT, RIGHT
    }
}
