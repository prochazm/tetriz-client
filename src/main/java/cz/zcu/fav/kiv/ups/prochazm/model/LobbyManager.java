package cz.zcu.fav.kiv.ups.prochazm.model;

import cz.zcu.fav.kiv.ups.prochazm.model.beans.RoomBean;
import cz.zcu.fav.kiv.ups.prochazm.networking.Protocol;
import javafx.beans.Observable;
import javafx.collections.MapChangeListener;
import javafx.collections.ObservableList;
import javafx.collections.ObservableMap;

import java.util.HashSet;
import java.util.Set;

import static javafx.collections.FXCollections.*;

public class LobbyManager {

    private final Protocol protocol;

    private final ObservableMap<Long, RoomBean> idRoomBeanMap;
    private final ObservableList<RoomBean> roomBeans;

    public LobbyManager(Protocol protocol) {
        this.protocol = protocol;
        this.idRoomBeanMap = synchronizedObservableMap(observableHashMap());
        this.roomBeans = synchronizedObservableList(
            observableArrayList(
                bean -> new Observable[] {
                    bean.playerCountProperty(),
                    bean.playerLimitProperty()
                }
        ));

        this.idRoomBeanMap.addListener((MapChangeListener<? super Long, ? super RoomBean>) change -> {
            if (change.wasAdded()){
                this.roomBeans.add(change.getValueAdded());
            } else {
                this.roomBeans.remove(change.getValueRemoved());
            }
        });

    }

    public ObservableList<RoomBean> getRoomBeans(){
        return unmodifiableObservableList(this.roomBeans);
    }

    public void updateAll() {
        Set<Long> newIds = new HashSet<>();

        // make this atomic, doesn't need to be but makes it easier to debug
        synchronized (this.protocol) {
            this.protocol.queryRoomsIds(id -> {
                newIds.add(id);
                protocol.queryRoom(idRoomBeanMap.computeIfAbsent(id, RoomBean::new));
            });
        }

        this.idRoomBeanMap.keySet().retainAll(newIds);
    }

    private void updateSingle(RoomBean roomBean) {
        if (!this.protocol.queryRoom(roomBean)){
            this.idRoomBeanMap.remove(roomBean.getId());
        }
    }

    public boolean join(RoomBean roomBean) {
        return this.protocol.joinRoom(roomBean);
    }

    public RoomBean create(int playerLimit, String owner) {
        RoomBean roomBean = new RoomBean(0, 0, playerLimit, owner);
        boolean result = this.protocol.createRoom(roomBean);

        if (result) {
           this.idRoomBeanMap.put(roomBean.getId(), roomBean);
        }

        return roomBean;
    }

    public boolean leave(RoomBean roomBean) {
        return roomBean != null && this.protocol.leaveRoom();
    }

    public RoomBean getMyRoom() {
        long roomId = this.protocol.getMyRoomId();
        if (roomId == 0) {
            return null;
        }

        RoomBean roomBean = new RoomBean(roomId);
        updateSingle(roomBean);

        return roomBean;
    }
}
